import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/models/user_settings.dart';

class AppUser {
  final String id;
  final String accessCode;
  final bool active;
  final Map availableOn;
  final String clinicId;
  final int connectyCubeId;
  final String pharmacyId;
  final DateTime dateEnrolled;
  final DateTime dateJoined;
  final String email;
  final bool enrolled;
  final String firstName;
  final bool isMedic;
  final bool isPharmacist;
  final bool isTrialUser;
  final String lastName;
  final String medicalId;
  final String orgId;
  final UserSettings settings;
  final String phone;
  final String photo;
  final bool subscribed;
  final String username;

  AppUser({
    this.id,
    this.accessCode,
    this.active,
    this.availableOn,
    this.clinicId,
    this.connectyCubeId,
    this.pharmacyId,
    this.dateEnrolled,
    this.dateJoined,
    this.email,
    this.enrolled,
    this.firstName,
    this.isMedic,
    this.isPharmacist,
    this.isTrialUser,
    this.lastName,
    this.medicalId,
    this.orgId,
    this.settings,
    this.phone,
    this.photo,
    this.subscribed,
    this.username,
  });

  factory AppUser.fromDoc(DocumentSnapshot doc) {
    Map data = doc.data();

    return AppUser(
      id: doc.id,
      active: data['active'] ?? false,
      availableOn: data['availableOn'],
      clinicId: data['assignedClinicId'] ?? '',
      connectyCubeId: data['connectyCubeId'],
      pharmacyId: data['assignedPharmacyId'] ?? '',
      dateEnrolled: data['dateEnrolled'] != null
          ? data['dateEnrolled'].toDate()
          : DateTime.now(),
      dateJoined: (data['dateJoined'] != null)
          ? data['dateJoined'].toDate()
          : DateTime.now(),
      email: data['email'] ?? '',
      enrolled: data['enrolled'] ?? false,
      firstName: data['firstName'] ?? '',
      isMedic: data['isMedic'] ?? false,
      isPharmacist: data['isPharmacist'] ?? false,
      isTrialUser: data['isTrialUser'] ?? false,
      lastName: data['lastName'] ?? '',
      medicalId: data['medicalId'] ?? '',
      orgId: data['orgId'] ?? '',
      settings: data['settings'] != null ? UserSettings.fromJson(data['settings']) : null,
      photo: data['profilePhoto'] ??
          "https://i.pinimg.com/originals/7c/4b/d8/7c4bd89875c1180dee6f737ff969f908.jpg",
      subscribed: data['subscribed'] ?? false,
      username: data['username'] ?? '',
    );
  }

  String fullname() {
    return firstName + " " + lastName;
  }
}
