import 'package:cloud_firestore/cloud_firestore.dart';

class Organization {
  final String id;
  final String adminEmail;
  final String city;
  final String country;
  final DateTime dateJoined;
  final String designation;
  final String name;
  final String state;

  Organization({
    this.id,
    this.adminEmail,
    this.city,
    this.country,
    this.dateJoined,
    this.designation,
    this.name,
    this.state,
  });

  // Create New
  // Create from Existing

  factory Organization.fromDoc(DocumentSnapshot doc) {
    Map data = doc.data();

    return Organization(
      id: doc.id,
      adminEmail: data['adminEmail'] ?? '',
      city: data['city'] ?? '',
      country: data['country'] ?? '',
      dateJoined: data['dateJoined'] != null
          ? data['dateJoined'].toDate()
          : DateTime.now(),
      designation: data['designation'] ?? '',
      name: data['name'] ?? '',
      state: data['state'] ?? ''
    );
  }
}
