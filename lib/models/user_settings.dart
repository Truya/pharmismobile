class UserSettings {
  int timePerSlot;
  int unitsPerSlot;

  UserSettings({this.timePerSlot, this.unitsPerSlot});

   UserSettings.fromJson(Map<String, dynamic> json) {
    timePerSlot = json['timePerSlot'] ?? 10;
    unitsPerSlot = json['unitsPerSlot'] ?? 10;
  }

  toJson(){
    return {
      "timePerSlot": timePerSlot,
      "unitsPerSlot": unitsPerSlot,
    };
  }
}
