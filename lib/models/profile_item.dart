import 'package:flutter/material.dart';

class ProfileItem {
  final Icon icon;
  final String title;

  ProfileItem({
    this.icon,
    this.title,
  });
}

List<ProfileItem> profileItems = [
  ProfileItem(
    icon: Icon(
      Icons.credit_card,
      color: Colors.white,
    ),
    title: 'Payments',
  ),
  ProfileItem(
    icon: Icon(
      Icons.calendar_today_outlined,
      color: Colors.white,
    ),
    title: 'Appointments',
  ),
  ProfileItem(
    icon: Icon(
      Icons.phone_outlined,
      color: Colors.white,
    ),
    title: 'Call Logs',
  ),
  ProfileItem(
    icon: Icon(
      Icons.settings_outlined,
      color: Colors.white,
    ),
    title: 'Settings',
  ),
  ProfileItem(
    icon: Icon(
      Icons.lock_outline,
      color: Colors.white,
    ),
    title: 'Reset Password',
  ),
];
