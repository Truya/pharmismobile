class Medication {
  final String itemDescription;
  final String dosage;
  final String frequency;
  final DateTime startDate;
  final DateTime endDate;

  Medication({
    this.itemDescription,
    this.dosage,
    this.frequency,
    this.startDate,
    this.endDate,
  });

  factory Medication.fromJson(Map<String, dynamic> json) {
    return Medication(
      itemDescription: json['itemDescription'] ?? '',
      dosage: json['dosage'],
      frequency: json['frequency'],
      startDate: json['startDate'] != null
          ? json['startDate'].toDate()
          : DateTime.now(),
      endDate:
          json['endDate'] != null ? json['endDate'].toDate() : DateTime.now(),
    );
  }

  toJson() {
    return {
      "itemDescription": itemDescription,
      "dosage": dosage,
      "frequency": frequency,
      "startDate": startDate,
      "endDate": endDate,
    };
  }
}
