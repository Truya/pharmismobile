import 'available_at.dart';

class Availability {
  List<AvailableAt> hoursAvailable;
  int utcOffset;

  Availability({
    this.hoursAvailable,
    this.utcOffset,
  });

  Availability.fromJson(Map<String, dynamic> json) {
    hoursAvailable = (json['hoursAvailable'] != null)
        ? json['hoursAvailable'].map<AvailableAt>((doc) {
            return AvailableAt.fromJson(doc);
          }).toList()
        : [];
    utcOffset = json['utcOffset'] ?? 0;
  }

  toJson() {
    return {
      "hoursAvailable": hoursAvailable,
      "utcOffset": utcOffset,
    };
  }
}
