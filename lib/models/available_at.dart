import 'package:flutter/material.dart';
import 'package:truya_pharmis/util/utils.dart';

class AvailableAt {
  String startsAt;
  String endsAt;

  TimeOfDay start;
  TimeOfDay end;

  AvailableAt({
    this.startsAt,
    this.endsAt,
    this.start,
    this.end,
  });

  AvailableAt.fromJson(Map<String, dynamic> json) {
    startsAt = json['startsAt'];
    endsAt = json['endsAt'];
    start = Utils.getTodFromDateTime(json['start'].toDate());
    end = Utils.getTodFromDateTime(json['end'].toDate());
  }

  toJson(){
    return {
      "startsAt": startsAt,
      "endsAt": endsAt
    };
  }

  title() {
    return start != null && end != null
        ? "${Utils.formatTime(start)} - ${Utils.formatTime(end)}"
        : null;
  }

  TimeOfDay startTime() {
    return startsAt != null
        ? Utils.stringToTimeOfDay(startsAt)
        : TimeOfDay.now();
  }

  TimeOfDay endTime() {
    return endsAt != null ? Utils.stringToTimeOfDay(endsAt) : TimeOfDay.now();
  }
}
