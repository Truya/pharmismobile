import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/models/medication.dart';

class Prescription {
  final String id;
  final bool active;
  final String caseNo;
  final String clinicId;
  final String description;
  final String fulfillerId; // pharm
  final String fulfillmentId;
  final DateTime fulfillmentDate;
  final DateTime issueDate;
  final String issuerId; // doc
  final List<dynamic> medications;
  final String notes;
  final String pharmacistId;
  final String prescriptionId;
  final String recipientId;
  final String relatedTo;
  final String status;

  Prescription({
    this.id,
    this.active,
    this.caseNo,
    this.clinicId,
    this.description,
    this.fulfillerId,
    this.fulfillmentId,
    this.fulfillmentDate,
    this.issueDate,
    this.issuerId,
    this.medications,
    this.notes,
    this.pharmacistId,
    this.prescriptionId,
    this.recipientId,
    this.relatedTo,
    this.status,
  });

  // Create New
  // Create from Existing

  factory Prescription.fromDoc(DocumentSnapshot doc) {
    Map data = doc.data();

    return Prescription(
      id: doc.id,
      active: data['active'] ?? false,
      caseNo: data['caseNo'] ?? '',
      clinicId: data['clinicId'] ?? '',
      description: data['description'] ?? '',
      fulfillerId: data['fulfillerId'] ?? '',
      fulfillmentId: data['fulfillmentId'] ?? '',
      fulfillmentDate: data['fulfillmentDate'] != null
          ? data['fulfillmentDate'].toDate()
          : DateTime.now(),
      issueDate: data['issueDate'] != null
          ? data['issueDate'].toDate()
          : DateTime.now(),
      issuerId: data['issuerId'] ?? '',
      medications: (data['medications'] != null)
          ? data['medications'].map<Medication>((doc) {
              return Medication.fromJson(doc);
            }).toList()
          : [],
      notes: data['notes'] ?? '', 
      pharmacistId: data['pharmacistId'] ?? '',
      recipientId: data['recipientId'] ?? '',
      relatedTo: data['relatedTo'] ?? '',
      status: data['status'] ?? "ISSUED",
    );
  }

  toJson() {
    return {
      "issuerId": issuerId,
      "issueDate": FieldValue.serverTimestamp(),
      "clinicId": clinicId,
      "recipientId": recipientId,
      "medications": medications ?? [],
      "description": description,
      "notes": notes,
      "relatedTo": relatedTo ?? null,
      "caseNo": caseNo,
      "status": "ISSUED",
      "active": true,
    };
  }
}
