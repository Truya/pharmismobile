import 'package:cloud_firestore/cloud_firestore.dart';

class RefillRequest {
  final String id;
  final String clinicId;
  final String comments;
  final String issuerId;
  final DateTime lastStatusChange;
  final String prescriptionId;
  final String refillRequestId;
  final DateTime requestDate;
  String status;
  final String userId;

  RefillRequest({
    this.id,
    this.clinicId,
    this.comments,
    this.issuerId,
    this.lastStatusChange,
    this.prescriptionId,
    this.refillRequestId,
    this.requestDate,
    this.status,
    this.userId,
  });

  factory RefillRequest.fromDoc(DocumentSnapshot doc) {
    Map data = doc.data();

    return RefillRequest(
      id: doc.id,
      clinicId: data['clinicId'] ?? '',
      comments: data['comments'] ?? '',
      issuerId: data['issuerId'] ?? '',
      lastStatusChange: data['lastStatusChange'] != null
          ? data['lastStatusChange'].toDate()
          : DateTime.now(),
      prescriptionId: data['prescriptionId'] ?? '',
      refillRequestId: data['refillRequestId'] ?? '',
      requestDate: data['requestDate'] != null
          ? data['requestDate'].toDate()
          : DateTime.now(),
      status: data['status'] ?? 'PENDING',
      userId: data['userId'] ?? '',
    );
  }

  toJson() {
    return {
      "clinicId": clinicId,
      "comments": comments,
      "issuerId": issuerId,
      "lastStatusChange": FieldValue.serverTimestamp(),
      "prescriptionId": prescriptionId,
      "requestDate": FieldValue.serverTimestamp(),
      "status": "PENDING",
      "userId": userId,
    };
  }
}
