import 'package:cloud_firestore/cloud_firestore.dart';

class Appointment {
  final String id;
  final String cancellationReason;
  final DateTime cancelledAt;
  final DateTime createdAt;
  final String doctorId;
  final DateTime endsAt;
  final String patientId;
  final DateTime startsAt;
  final String
      status; // PENDING, COMPLETED, DOCTOR_CANCELLED, PATIENT_CANCELLED

  Appointment({
    this.id,
    this.cancellationReason,
    this.cancelledAt,
    this.createdAt,
    this.doctorId,
    this.endsAt,
    this.patientId,
    this.startsAt,
    this.status,
  });

  factory Appointment.fromJson(Map<String, dynamic> json) {
    return Appointment(
      cancellationReason: json['cancellationReason'] ?? '',
      cancelledAt: json['cancelledAt'] != null
          ? json['endDate'].toDate()
          : DateTime.now(),
      createdAt: json['createdAt'] != null
          ? json['createdAt'].toDate()
          : DateTime.now(),
      doctorId: json['doctorId'],
      endsAt: json['endsAt'] != null ? json['endsAt'].toDate() : DateTime.now(),
      patientId: json['patientId'],
      startsAt:
          json['startsAt'] != null ? json['startsAt'].toDate() : DateTime.now(),
      status: json['status'],
    );
  }

  factory Appointment.fromDoc(DocumentSnapshot doc) {
    Map data = doc.data();

    return Appointment(
      id: doc.id,
      cancellationReason: data['cancellationReason'] ?? '',
      cancelledAt: data['cancelledAt'] != null
          ? data['endDate'].toDate()
          : DateTime.now(),
      createdAt: data['createdAt'] != null
          ? data['createdAt'].toDate()
          : DateTime.now(),
      doctorId: data['doctorId'],
      endsAt: data['endsAt'] != null ? data['endsAt'].toDate() : DateTime.now(),
      patientId: data['patientId'],
      startsAt:
          data['startsAt'] != null ? data['startsAt'].toDate() : DateTime.now(),
      status: data['status'],
    );
  }

  toJson() {
    return {
      // "cancellationReason": cancellationReason,
      "createdAt": FieldValue.serverTimestamp(),
      "doctorId": doctorId,
      "endsAt": endsAt,
      "patientId": patientId,
      "startsAt": startsAt,
      "status": "PENDING",
    };
  }

  String getStatus() {
    List<String> s = status.split("_");

    if (s.length == 1) {
      return status;
    } else {
      return s[0] + " " + s[1];
    }
  }

  bool isCancelled() {
    List<String> s = status.split("_");

    if (s.length == 1) {
      return false;
    } else {
      return true;
    }
  }
}
