import 'package:cloud_firestore/cloud_firestore.dart';

class CallLog {
  String id;
  String doctorId;
  String patientId;
  DateTime startTime;
  DateTime endTime;
  String status; // accepted or rejected

  CallLog({
    this.id,
    this.doctorId,
    this.patientId,
    this.startTime,
    this.endTime,
    this.status,
  });

  factory CallLog.fromDoc(DocumentSnapshot doc) {
    Map data = doc.data();

    return CallLog(
      id: doc.id,
      doctorId: data['doctorId'] ?? '',
      patientId: data['patientId'] ?? '',
      startTime: data['startTime'] != null
          ? data['startTime'].toDate()
          : DateTime.now(),
      endTime:
          data['endTime'] != null ? data['endTime'].toDate() : DateTime.now(),
      status: data['status'] ?? 'pending',
    );
  }

  toJson() {
    return {
      "startTime": FieldValue.serverTimestamp(),
      "doctorId": doctorId,
      "patientId": patientId,
      "status": "pending",
    };
  }
}
