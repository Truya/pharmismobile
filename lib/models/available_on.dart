import 'availability.dart';


class AvailableOn {
  Availability monday;
  Availability tuesday;
  Availability wednesday;
  Availability thursday;
  Availability friday;
  Availability saturday;
  Availability sunday;

  AvailableOn({
    this.monday,
    this.tuesday,
    this.wednesday,
    this.thursday,
    this.friday,
    this.saturday,
    this.sunday,
  });

  AvailableOn.fromJson(Map<String, dynamic> json) {
    monday = json['monday'] != null ? new Availability.fromJson(json['monday']) : null;
    tuesday = json['tuesday'] != null ? new Availability.fromJson(json['tuesday']) : null;
    wednesday = json['wednesday'] != null ? new Availability.fromJson(json['wednesday']) : null;
    thursday = json['thursday'] != null ? new Availability.fromJson(json['thursday']) : null;
    friday = json['friday'] != null ? new Availability.fromJson(json['friday']) : null;
    saturday = json['saturday'] != null ? new Availability.fromJson(json['saturday']) : null;
    sunday = json['sunday'] != null ? new Availability.fromJson(json['sunday']) : null;
  }
}
