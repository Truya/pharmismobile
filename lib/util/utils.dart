import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

class Utils {
  static Widget verticalSpacer({double space = 20.0}) {
    return SizedBox(height: space);
  }

  static Widget horizontalSpacer({double space = 20.0}) {
    return SizedBox(width: space);
  }

  static String formatDateTime(DateTime dateTime) {
    return DateFormat('MMM dd, yyyy hh:mm a').format(dateTime);
  }

  static String formatDate(DateTime dateTime) {
    return DateFormat('MMM dd, yyyy').format(dateTime);
  }

  static TimeOfDay stringToTimeOfDay(String tod) {
    // 24 hours to tod
    // final format = DateFormat.jm(); //"6:00 AM"
    // return TimeOfDay.fromDateTime(format.parse(tod));
    TimeOfDay time = TimeOfDay(
      hour: int.parse(tod.split(":")[0]),
      minute: int.parse(tod.split(":")[1]),
    );
    return time;
  }

  static TimeOfDay stringToTimeOfDay2(String tod) {
    // Actual tod
    final format = DateFormat.jm(); //"6:00 AM"
    return TimeOfDay.fromDateTime(format.parse(tod));
  }

  static String formatTime(TimeOfDay tod) {
    final now = new DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    final format = DateFormat.jm(); //"6:00 AM"
    return format.format(dt);
  }

  static String todTo24h(TimeOfDay tod) {
    var df = DateFormat.jm();
    var dt = df.parse(formatTime(tod));
    return DateFormat('HH:mm').format(dt);
  }

  static DateTime todToToday(TimeOfDay tod) {
    var df = DateFormat.jm();
    return df.parse(formatTime(tod));
  }

  static String todToTodayString(TimeOfDay tod) {
    var df = DateFormat.jm();
    var dt = df.parse(formatTime(tod));
    return DateFormat('MMM dd, yyyy HH:mm').format(dt);
  }

  static DateTime completeTod(TimeOfDay tod, {DateTime date}) {
    var format = DateFormat('yyy-MM-dd HH:mm:ss');
    var now = date == null ? new DateTime.now() : date;
    var _date = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    return format.parse(format.format(_date));
  }

  static String getOnlyTime(DateTime dateTime) {
    return DateFormat.jm().format(dateTime);
  }

  static TimeOfDay getTodFromDateTime(DateTime dateTime) {
    return TimeOfDay.fromDateTime(dateTime);
  }

  static bool isSameDate(DateTime firstDate, DateTime secondDate) {
    return firstDate.year == secondDate.year &&
        firstDate.month == secondDate.month &&
        firstDate.day == secondDate.day;
  }

  static List<String> daysOfWeek() {
    return [
      "monday",
      "tuesday",
      "wednesday",
      "thursday",
      "friday",
      "saturday",
      "sunday"
    ];
  }
}
