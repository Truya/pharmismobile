import 'dart:io';

import 'package:flutter/material.dart';
import 'package:truya_pharmis/constants/strings.dart';

class ThemeConfig {
  //Colors for theme
  static Color primaryColor = Color(0xFF48D000);
  static Color lightPrimary = Color(0xfffcfcff);
  static Color darkPrimary = Color(0xff1f1f1f);
  static Color lightAccent = primaryColor; // Color(0xff0078D7);
  static Color darkAccent = primaryColor; // Color(0xff0078D7);
  static Color lightBG = Color(0xfffcfcff);
  static Color darkBG = Color(0xff121212);

  static ThemeData lightTheme = ThemeData(
    fontFamily: primaryFontFamily,
    backgroundColor: lightBG,
    primaryColor: lightPrimary,
    accentColor: lightAccent,
    scaffoldBackgroundColor: lightBG,
    textTheme: TextTheme(
      headline6: TextStyle(
        color: Color(0xff4D4D4D),
      ),
      bodyText2: TextStyle(
        color: Colors.grey,
      ),
    ),
    textSelectionTheme: TextSelectionThemeData(cursorColor: lightAccent),
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {
        TargetPlatform.android: ZoomPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      },
    ),
    colorScheme: ColorScheme.light(
      primary: Colors.white,
      onPrimary: Colors.black87,
      primaryVariant: Colors.white38,
    ),
    appBarTheme: AppBarTheme(
      brightness: Platform.isIOS ? Brightness.light : Brightness.dark,
      color: lightPrimary,
      elevation: 0,
      iconTheme: IconThemeData(
        color: Colors.black54,
      ),
      textTheme: TextTheme(
        headline6: TextStyle(
          color: Colors.black54,
          fontFamily: primaryFontFamily,
        ),
      ),
    ),
  );

  static ThemeData darkTheme = ThemeData(
    fontFamily: primaryFontFamily,
    brightness: Brightness.dark,
    backgroundColor: darkBG,
    primaryColor: darkPrimary,
    accentColor: darkAccent,
    scaffoldBackgroundColor: darkBG,
    textSelectionTheme: TextSelectionThemeData(cursorColor: lightAccent),
    pageTransitionsTheme: PageTransitionsTheme(
      builders: {
        TargetPlatform.android: ZoomPageTransitionsBuilder(),
        TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
      },
    ),
    colorScheme: ColorScheme.light(
      primary: Colors.black38,
      onPrimary: Colors.white70,
      primaryVariant: Colors.black,
      brightness: Brightness.dark,
    ),
    textTheme: TextTheme(
      bodyText2: TextStyle(
        color: Colors.white24,
      ),
    ),
    appBarTheme: AppBarTheme(
      brightness: Brightness.dark,
      color: darkPrimary,
      elevation: 0,
      textTheme: TextTheme(
        headline6: TextStyle(
          color: Colors.white54,
          fontFamily: primaryFontFamily,
        ),
      ),
    ),
  );

  static Color profileCardShadowColor = Colors.grey[200];

  static BoxShadow cardShadow = BoxShadow(
    color: Colors.grey[300].withOpacity(0.8),
    blurRadius: 8.0,
    spreadRadius: 0.0,
    offset: Offset(
      0.0,
      2.0,
    ),
  );
}
