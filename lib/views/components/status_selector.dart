import 'package:flutter/material.dart';

class StatusSelector extends StatefulWidget {
  final List<String> statusList;
  final String selectedStatus;
  final ValueChanged<String> onChanged;

  const StatusSelector({
    Key key,
    @required this.statusList,
    @required this.selectedStatus,
    this.onChanged,
  }) : super(key: key);

  @override
  _StatusSelectorState createState() => _StatusSelectorState();
}

class _StatusSelectorState extends State<StatusSelector> {
   String selected;

   @override
  void initState() {
    setState(() {
      selected = widget.selectedStatus;
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      padding: EdgeInsets.all(10.0),
      margin: EdgeInsets.only(bottom: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(8.0),
        ),
        border: Border.all(color: Colors.grey.withOpacity(0.3)),
      ),
      child: DropdownButton(
        underline: Container(),
        isExpanded: true,
        hint: Text("Change Status"),
        value: selected,
        items: widget.statusList.map((item) {
          return DropdownMenuItem(
            child: Text(
              item,
            ),
            value: item,
          );
        }).toList(),
        onChanged: (value) {
          widget.onChanged(value);
          setState(() {
            selected = value;
          });
        },
      ),
    );
  }
}
