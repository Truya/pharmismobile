import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/util/utils.dart';

class UserPrescriptionTile extends StatelessWidget {
  final Prescription prescription;
  final int index;
  final VoidCallback onTap;

  const UserPrescriptionTile({
    Key key,
    this.prescription,
    this.index,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = 30.0;
    int count = index + 1;
    return MaterialButton(
      onPressed: onTap,
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        leading: Container(
          height: size,
          width: size,
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: Center(
            child: Text(
              count.toString(),
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        trailing: statusBadge(
          prescription.status,
        ),
        title: Text(
          Utils.formatDateTime(prescription.issueDate),
        ),
        subtitle: Text("${prescription.medications.length} med(s)"),
      ),
    );
  }

  Widget statusBadge(status) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
      decoration: BoxDecoration(
        color: status == "ISSUED"
            ? Colors.grey
            : status == "FULFILLED"
                ? Colors.green
                : Colors.red,
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Text(
        status,
        style: TextStyle(fontSize: 12.0, color: Colors.white),
      ),
    );
  }

  Widget optionsTile({
    IconData icon,
    String title,
    VoidCallback onTap,
  }) {
    return ListTile(
      onTap: onTap,
      leading: Icon(icon),
      title: Text(title),
    );
  }
}
