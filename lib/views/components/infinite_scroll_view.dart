import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class InfiniteScrollView extends StatefulWidget {
  final List data;
  final bool isList;
  final IndexedWidgetBuilder itemBuilder;
  final SliverGridDelegate gridDelegate;
  final VoidCallback onScrollEnd;
  final bool isLoading;
  final RefreshCallback onRefresh;
  final bool shrinkWrap;

  const InfiniteScrollView({
    Key key,
    @required this.data,
    this.isList = true,
    @required this.itemBuilder,
    this.gridDelegate,
    @required this.onScrollEnd,
    this.isLoading = false,
    this.onRefresh,
    this.shrinkWrap = true,
  }) : super(key: key);

  @override
  _InfiniteScrollViewState createState() => _InfiniteScrollViewState();
}

class _InfiniteScrollViewState extends State<InfiniteScrollView> {
  ScrollController _scrollController = new ScrollController();

  bool onScrollEnd(ScrollNotification notification) {
    if (notification is ScrollUpdateNotification) {
      double maxScrollExtent = _scrollController.position.maxScrollExtent;
      double offset = _scrollController.offset;
      if (maxScrollExtent > offset && maxScrollExtent - offset <= 50) {
        if (widget.onScrollEnd != null) {
          widget.onScrollEnd();
        }
      }
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final view = _buildView();

    return NotificationListener(
      onNotification: onScrollEnd,
      child: widget.onRefresh == null
          ? view
          : RefreshIndicator(
              onRefresh: widget.onRefresh,
              child: view,
            ),
    );
  }

  Widget _buildView() {
    if (widget.isList) {
      return _buildList();
    } else {
      return _buildGrid();
    }
  }

  Widget _buildGrid() {
    return GridView.builder(
      controller: _scrollController,
      physics: const AlwaysScrollableScrollPhysics(),
      itemCount: widget.isLoading ? widget.data.length + 1 : widget.data.length,
      gridDelegate: widget.gridDelegate,
      itemBuilder: (context, index) {
        if (index == widget.data.length) {
          return VxSkeleton();
        }
        return widget.itemBuilder(context, index);
      },
      padding: EdgeInsets.only(left: 1.0, right: 1.0, bottom: 50.0, top: 20.0),
    );
  }

  Widget _buildList() {
    return ListView.separated(
      shrinkWrap: widget.shrinkWrap,
      controller: _scrollController,
      physics: const AlwaysScrollableScrollPhysics(),
      itemCount: widget.isLoading ? widget.data.length + 1 : widget.data.length,
      itemBuilder: (context, index) {
        if (index == widget.data.length) {
          return VxSkeleton();
        }
        return widget.itemBuilder(context, index);
      },
      separatorBuilder: (BuildContext context, int index) {
        return Divider();
      },
      padding: EdgeInsets.only(top: 10.0),
    );
  }
}
