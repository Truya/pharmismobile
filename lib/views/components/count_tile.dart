import 'package:flutter/material.dart';

class CountTile extends StatelessWidget {
  final int count;

  const CountTile({Key key, this.count}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = 30.0;
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey,
        ),
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Center(
        child: Text(
          count.toString(),
          style: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
