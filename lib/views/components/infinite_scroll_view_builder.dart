import 'package:flutter/material.dart';
import 'package:truya_pharmis/constants/images.dart';
import 'empty_state.dart';
import 'infinite_scroll_view.dart';
import 'shimmer_list.dart';

class InfiniteScrollViewBuilder extends StatelessWidget {
  final List data;
  final bool isList;
  final IndexedWidgetBuilder itemBuilder;
  final SliverGridDelegate gridDelegate;
  final VoidCallback onScrollEnd;
  final bool isLoading;
  final RefreshCallback onRefresh;
  final bool shrinkWrap;
  final String emptyMessage;
  final String emptyImage;
  final Widget emptyWidget;

  const InfiniteScrollViewBuilder({
    Key key,
    @required this.data,
    this.isList = true,
    @required this.itemBuilder,
    this.gridDelegate,
    @required this.onScrollEnd,
    this.isLoading = false,
    this.onRefresh,
    this.shrinkWrap = true,
    this.emptyMessage = "No data to show",
    this.emptyImage = AppImages.empty,
    this.emptyWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _buildView(),
    );
  }

  Widget _buildView() {
    if (isLoading) {
      return ShimmerList(
        isList: isList,
        gridDelegate: gridDelegate,
      );
    } else {
      if (data.length == 0) {
        
        var view = EmptyState(
          message: emptyMessage,
          image: emptyImage,
        );
        return onRefresh == null
            ? view
            : RefreshIndicator(
                onRefresh: onRefresh,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 50.0,
                    ),
                    view,
                  ],
                ),
              );
      } else if (data.length > 0) {
        return InfiniteScrollView(
          data: data,
          isList: isList,
          isLoading: isLoading,
          onScrollEnd: onScrollEnd,
          itemBuilder: itemBuilder,
          gridDelegate: gridDelegate,
          onRefresh: onRefresh,
        );
      } else {
        return Container();
      }
    }
  }
}
