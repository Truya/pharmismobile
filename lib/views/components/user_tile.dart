import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/views/components/circular_image.dart';

class UserTile extends StatelessWidget {
  final AppUser user;
  final VoidCallback onTap;

  const UserTile({
    Key key,
    this.user,
    this.onTap,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return user != null ? ListTile(
      onTap: onTap,
      leading: CircularImage(
        image: user.photo,
      ),
      title: Text(user.fullname()),
    ) : Container();
  }
}
