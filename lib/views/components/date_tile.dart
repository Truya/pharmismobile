import 'package:flutter/material.dart';

class DateTile extends StatefulWidget {
  final DateTime date;

  const DateTile({
    Key key,
    this.date,
  }) : super(key: key);

  @override
  _DateTileState createState() => _DateTileState();
}

class _DateTileState extends State<DateTile> {
  Map<int, String> monthsInYear = {
    1: "Jan",
    2: "Feb",
    3: "Mar",
    4: "Apr",
    5: "May",
    6: "Jun",
    7: "Jul",
    8: "Aug",
    9: "Sept",
    10: "Oct",
    11: "Nov",
    12: "Dec"
  };
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 70.0,
      width: 53.0,
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            widget.date.day.toString(),
            style: TextStyle(
              fontSize: 19.0,
              color: Colors.white,
            ),
          ),
          Text(
            monthsInYear[widget.date.month].toUpperCase(),
            style: TextStyle(
              fontSize: 12.0,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
