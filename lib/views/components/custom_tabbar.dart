import 'package:flutter/material.dart';

class CustomTabBar extends StatelessWidget {
  final TabController controller;
  final List<String> tabs;

  const CustomTabBar({
    Key key,
    @required this.controller,
    this.tabs,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: TabBar(
        labelColor: Theme.of(context).accentColor,
        indicatorColor: Theme.of(context).accentColor,
        unselectedLabelColor: Colors.grey.withOpacity(0.6),
        tabs: tabs.map((tab) {
          var index = tabs.indexOf(tab);
          return Tab(
            text: tabs[index],
          );
        }).toList(),
        controller: controller,
      ),
    );
  }
}
