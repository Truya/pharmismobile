import 'package:flutter/material.dart';

class CustomBtn extends StatelessWidget {
  final String text;
  final Function onPressed;
  final Color color;
  final Color textColor;
  final bool isOutline;
  final bool wrap;
  final double elevation;
  final Widget child;
  final bool disabled;
  final bool isLoading;

  const CustomBtn({
    Key key,
    @required this.text,
    @required this.onPressed,
    this.color,
    this.textColor,
    this.isOutline = false,
    this.wrap = false,
    this.elevation = 4.0,
    this.child,
    this.disabled = false,
    this.isLoading = false,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
     final double btnSize = 45.0;
    final br = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8.0),
    );

    final btnText = Center(
      child: Text(
        text.toUpperCase(),
        style: TextStyle(
          fontWeight: FontWeight.w800,
          color: textColor != null ? textColor : isOutline ? Theme.of(context).accentColor : Colors.white,
        ),
      ),
    );

    final btnContent = Visibility(
      visible: !isLoading,
      child: btnText,
      replacement: SizedBox(
        height: 20,
        width: 20,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          strokeWidth: 2,
        ),
      ),
    );

    final materialBtn = SizedBox(
      width: isLoading ? 58.0 : null,
      child: MaterialButton(
        shape: isLoading ? RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(30.0),
    ) : br,
        height: btnSize,
        color: color ?? Theme.of(context).accentColor,
        elevation: elevation,
        onPressed: disabled ? null : isLoading ? null : onPressed,
        child: child != null ? child : btnContent,
        disabledColor: Colors.grey.withOpacity(0.3),
      ),
    );

    final outlineBtn = ButtonTheme(
      height: 40.0,
      child: OutlineButton(
        shape: br,
        borderSide: BorderSide(color: color ?? Theme.of(context).accentColor),
        onPressed: onPressed,
        child: child != null ? child : btnContent,
      ),
    );

    return isOutline ? outlineBtn : materialBtn;
  }
}
