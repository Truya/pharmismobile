import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/view_models/find_user.vm.dart';
import 'package:truya_pharmis/views/components/custom_btn.dart';
import 'package:truya_pharmis/views/components/empty_state.dart';

import '../custom_form_field.dart';
import '../user_tile.dart';

class FindUserBottomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<FindUserViewModel>.reactive(
      viewModelBuilder: () => FindUserViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        // final height = MediaQuery.of(context).size.height * 0.8;
        final form = Container(
          child: Form(
            key: model.formKey,
            child: Column(
              children: [
                CustomFormField(
                  icon: FlutterIcons.mail_fea,
                  labelText: "Email Address",
                  controller: model.emailCtrl,
                  enabled: !model.isLoading,
                  validator: (String value) {
                    if (value.isEmpty || value == null)
                      return "Patient email is required.";

                    return null;
                  },
                  suffixIcon: model.isLoading
                      ? Container(
                        height: 1,
                        width: 1,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                              Theme.of(context).accentColor,
                            ),
                          ),
                        )
                      : IconButton(
                          icon: Icon(FlutterIcons.search_fea),
                          onPressed: () => model.find(),
                        ),
                ),
              ],
            ),
          ),
        );

        final userDetails = Visibility(
          visible: model.hasLoaded && model.notFound,
          child: Container(
            child: EmptyState(message: "User not found!"),
          ),
          replacement: Visibility(
            visible: model.hasLoaded && !model.notFound,
            child: Column(
              children: [
                UserTile(user: model.user),
                Utils.verticalSpacer(),
                CustomBtn(
                  text: "Continue",
                  onPressed: () => Navigator.of(context).pop(model.user),
                )
              ],
            ),
          ),
        );

        return Container(
          // height: model.hasLoaded ?  height : null,
          child: Column(
            children: [
              form,
              userDetails,
            ],
          ),
        );
      },
    );
  }
}
