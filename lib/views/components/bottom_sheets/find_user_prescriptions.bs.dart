import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/view_models/find_user_prescriptions.vm.dart';

import '../custom_form_field.dart';
import '../infinite_scroll_view_builder.dart';
import '../user_prescription_tile.dart';

class FindUserPrescriptionsBottomSheet extends StatelessWidget {
  final AppUser user;

  const FindUserPrescriptionsBottomSheet({
    Key key,
    this.user,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<FindUserPrescriptionsViewModel>.reactive(
      viewModelBuilder: () => FindUserPrescriptionsViewModel(),
      onModelReady: (model) => model.initialize(context, user),
      builder: (context, model, child) {
        final height = MediaQuery.of(context).size.height * 0.8;
        final form = Container(
          child: Form(
            key: model.formKey,
            child: Column(
              children: [
                CustomFormField(
                  icon: FlutterIcons.security_mco,
                  labelText: "Patient Access Code",
                  controller: model.codeCtrl,
                  enabled: !model.isLoading,
                  validator: (String value) {
                    if (value.isEmpty || value == null)
                      return "Patient access code is required.";

                    return null;
                  },
                  isPasswordField: true,
                  suffixIcon: model.isLoading
                      ? Container(
                          height: 1,
                          width: 1,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                              Theme.of(context).accentColor,
                            ),
                          ),
                        )
                      : IconButton(
                          icon: Icon(FlutterIcons.search_fea),
                          onPressed: () => model.find(),
                        ),
                ),
              ],
            ),
          ),
        );

        final list = Visibility(
          visible: model.hasLoaded,
          child: Container(
            height: MediaQuery.of(context).size.height * 0.7,
            child: InfiniteScrollViewBuilder(
              data: model.prescriptions,
              isLoading: model.isLoading,
              onScrollEnd: () => {},
              onRefresh: () async {},
              itemBuilder: (BuildContext context, int index) {
                return UserPrescriptionTile(
                  prescription: model.prescriptions[index],
                  index: index,
                  onTap: () => Navigator.pop(
                    context,
                    model.prescriptions[index],
                  ),
                );
              },
            ),
          ),
        );

        return Container(
          height: model.hasLoaded ? height : null,
          child: Column(
            children: [form, list],
          ),
        );
      },
    );
  }
}
