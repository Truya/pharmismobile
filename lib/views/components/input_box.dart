import 'package:flutter/material.dart';

class InputBox extends StatelessWidget {
  final bool enabled;
  final IconData icon;
  final Widget prefix;
  final double boxWidth, boxHeight;
  final String hintText, labelText, initialValue;
  final TextInputType textInputType;
  final TextEditingController controller;
  final TextInputAction textInputAction;
  final FocusNode focusNode, nextFocusNode;
  final VoidCallback submitAction;
  final Widget suffixIcon;
  final Color iconColor;
  final bool obscureText;
  final TextStyle textStyle;
  final FormFieldValidator<String> validateFunction;
  final void Function(String) onSaved, onChange;
  final Key key;

  InputBox(
      {this.key,
      this.prefix,
      this.boxHeight,
      this.boxWidth,
      this.controller,
      this.labelText,
      this.hintText,
      this.initialValue,
      this.obscureText = false,
      this.textInputType,
      this.textInputAction,
      this.focusNode,
      this.nextFocusNode,
      this.submitAction,
      this.suffixIcon,
      this.icon,
      this.iconColor,
      this.textStyle,
      this.validateFunction,
      this.onChange,
      this.onSaved,
      this.enabled});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$labelText',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontWeight: FontWeight.w600,
            fontSize: 16.0,
          ),
        ),
        SizedBox(height: 5.0),
        Container(
          height: 45,
          decoration: BoxDecoration(
            color: Color.fromRGBO(0, 120, 215, 0.03),
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: TextFormField(
              enabled: enabled,
              onChanged: onChange,
              style: TextStyle(
                  color: Theme.of(context).accentColor, fontSize: 12.0),
              maxLines: 1,
              autocorrect: false,
              key: key,
              initialValue: initialValue,
              controller: controller,
              obscureText: obscureText,
              keyboardType: textInputType,
              validator: validateFunction,
              onSaved: onSaved,
              cursorColor: Theme.of(context).accentColor,
              textInputAction: textInputAction,
              focusNode: focusNode,
              textCapitalization: (hintText.toLowerCase().contains('name'))
                  ? TextCapitalization.words
                  : TextCapitalization.none,
              onFieldSubmitted: (String term) {
                if (nextFocusNode != null) {
                  focusNode.unfocus();
                  FocusScope.of(context).requestFocus(nextFocusNode);
                } else {
                  submitAction();
                }
              },
              decoration: InputDecoration(
                hintText: hintText,
                hintStyle: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 12.0,
                  fontWeight: FontWeight.w600,
                ),
                border: InputBorder.none,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
