import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class ShimmerList extends StatelessWidget {
  final bool isList;
  final int count;

  /// This is required if type is grid
  final SliverGridDelegate gridDelegate;

  const ShimmerList({
    Key key,
    @required this.isList,
    this.count = 3,
    this.gridDelegate,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return _buildType();
  }

  Widget _buildType() {
    if (isList) {
       return _buildList();
    } else {
     
      return _buildGrid();
    }
  }

  Widget _buildList() {
    return ListView.separated(
      itemBuilder: (BuildContext context, int index) {
        return _buildWidgets()[index];
      },
      itemCount: count,
      separatorBuilder: (BuildContext context, int index) {
        return SizedBox(height: 10.0);
      },
      padding: EdgeInsets.only(top: 10.0),
    );
  }

  Widget _buildGrid() {
    return GridView(
      gridDelegate: gridDelegate,
      children: _buildWidgets(),
      padding: EdgeInsets.only(top: 10.0),
    );
  }

  List<Widget> _buildWidgets() {
    List widgets = new List<Widget>();

    int i = 0;
    for (i = 0; i < count; i++) {
      widgets.add(
        VxSkeleton(),
      );
    }
    return widgets;
  }
}
