import 'package:flutter/material.dart';

class BaseView extends StatelessWidget {
  final String title;
  final Widget body;
  final bool isLoading;
  final Widget fab;
  final bool givePadding;
  final double appBarElevation;
  final List<Widget> actions;

  const BaseView({
    Key key,
    this.title = "",
    @required this.body,
    this.isLoading = false,
    this.fab,
    this.givePadding = false,
    this.appBarElevation = 1,
    this.actions,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final linearLoading = LinearProgressIndicator(
      backgroundColor: Colors.white,
      valueColor: AlwaysStoppedAnimation<Color>(
        Theme.of(context).accentColor,
      ),
    );

    return Scaffold(
      floatingActionButton: fab,
      appBar: AppBar(
        elevation: appBarElevation,
        title: Text(
          title,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: actions,
      ),
      body: Container(
        padding: givePadding ? EdgeInsets.all(20) : EdgeInsets.zero,
        child: Column(
          children: [
            Visibility(
              visible: isLoading,
              child: linearLoading,
            ),
            Expanded(
              child: body,
            )
          ],
        ),
      ),
    );
  }
}
