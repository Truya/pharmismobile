import 'package:flutter/material.dart';

class CustomRow extends StatelessWidget {
  final String title;
  final String value;

  const CustomRow({
    Key key,
    @required this.title,
    @required this.value,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(title, style: TextStyle(color: Colors.black)),
          ),
          Expanded(
            child: Text(value, style: TextStyle(color: Colors.grey)),
          ),
        ],
      ),
    );
  }
}
