import 'package:flutter/material.dart';

class BsOptionTile extends StatelessWidget {
  final VoidCallback onTap;
  final IconData icon;
  final String title;

  const BsOptionTile({
    Key key,
    this.onTap,
    this.icon,
    this.title,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      leading: Icon(icon),
      title: Text(title),
    );
  }
}
