import 'package:flutter/material.dart';

class BsButton extends StatelessWidget {
  final String title;
  final VoidCallback onTap;

  const BsButton({
    Key key,
    this.title,
    this.onTap,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).accentColor,
      width: double.infinity,
      height: 70.0,
      child: InkWell(
        onTap: onTap,
        child: Center(
          child: Text(
            "Book Appointment".toUpperCase(),
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
