import 'package:flutter/material.dart';

class TextFieldTheme extends StatelessWidget {
  final Widget child;

  const TextFieldTheme({
    Key key,
    this.child,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        primarySwatch: Colors.blue,
      ),
      child: Builder(
        builder: (BuildContext context) {
          return child;
        },
      ),
    );
  }
}
