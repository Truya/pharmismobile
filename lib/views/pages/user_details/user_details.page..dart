import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/view_models/user_details.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/circular_image.dart';

class UserDetailsPage extends StatelessWidget {
  final AppUser user;

  const UserDetailsPage({Key key, this.user}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<UserDetailsViewModel>.reactive(
      viewModelBuilder: () => UserDetailsViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        // final contentBg = Colors.grey.withOpacity(0.05);
        // final iconColor = Colors.black87;
        // final headerTextStyle = TextStyle(
        //   color: Colors.black87,
        //   fontWeight: FontWeight.w600,
        // );

        // final basicDetails = Container(
        //   child: ExpandedTile(
        //     controller: ExpandedTileController(),
        //     leading: Icon(Icons.info_outline, color: iconColor),
        //     centerHeaderTitle: false,
        //     title: Text("Basic Info", style: headerTextStyle),
        //     content: Column(
        //       children: <Widget>[
        //         CustomRow(
        //           title: "First Name",
        //           value: user.firstName,
        //         ),
        //         CustomRow(
        //           title: "Last Name",
        //           value: user.lastName,
        //         ),
        //         Visibility(
        //           child: CustomRow(
        //             title: "Medica ID",
        //             value: user.medicalId,
        //           ),
        //         ),
        //       ],
        //     ),
        //     contentBackgroundColor: contentBg,
        //   ),
        // );

        final userDetails = Container(
          margin: EdgeInsets.all(20.0),
          child: Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(color: Colors.white),
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircularImage(
                  size: 100,
                  image: user.photo,
                ),
                Utils.verticalSpacer(space: 10),
                Text(
                  "Dr. ${user.fullname()}",
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1.color,
                    fontSize: 17.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  "Lagos Specialist Hospital",
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText2.color,
                    fontSize: 14.0,
                  ),
                ),
                Utils.verticalSpacer(space: 3),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  child: Text(
                    "Dentist",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12.0,
                    ),
                  ),
                )
              ],
            ),
          ),
        );

        final actions = Container();

        return BaseView(
          isLoading: model.isLoading,
          title: "User Details",
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                userDetails,
                actions
                // basicDetails,
              ],
            ),
          ),
        );
      },
    );
  }
}
