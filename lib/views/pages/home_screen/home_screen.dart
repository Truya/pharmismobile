import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/constants/images.dart';
import 'package:truya_pharmis/widgets/category_tile.dart';
import 'package:truya_pharmis/widgets/top_doctor_card.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(
          Icons.menu,
          color: Theme.of(context).accentColor,
        ),
        title: Text(
          'Good morning, \nBashir',
          style: TextStyle(
            fontSize: 14,
            height: 1.25,
            color: Colors.grey[600],
          ),
        ),
        actions: [
          CircleAvatar(
            backgroundColor: Theme.of(context).accentColor,
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 10,
              ),
              Text(
                'Find Your \nDesired Doctor',
                style: TextStyle(
                  fontSize: 30,
                  fontWeight: FontWeight.w600,
                  color: Theme.of(context).accentColor,
                ),
              ),
              Container(
                height: 10,
              ),
              Container(
                height: 45,
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Row(
                          children: [
                            SizedBox(
                              width: 20,
                            ),
                            Expanded(
                              child: TextField(
                                decoration: InputDecoration.collapsed(
                                  hintText: 'Search',
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      right: 0,
                      child: Container(
                        height: 45,
                        width: 80,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Center(
                          child: Icon(
                            Feather.search,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 20,
              ),
              Text(
                'Categories',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.grey[600],
                  fontWeight: FontWeight.w700,
                ),
              ),
              Container(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CategoryTile(
                    label: 'Dental Surgeon',
                    image: AppImages.dentalSurgeon,
                  ),
                  CategoryTile(
                    label: 'Heart Surgeon',
                    image: AppImages.heartSurgeon,
                  ),
                  CategoryTile(
                    label: 'Eye Specialist',
                    image: AppImages.eyeSpecialist,
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Top Doctors',
                style: TextStyle(
                  fontSize: 16,
                  color: Theme.of(context).accentColor,
                  fontWeight: FontWeight.w700,
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                itemCount: 5,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return TopDoctorCard();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
