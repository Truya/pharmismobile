import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/view_models/request_refill.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/custom_btn.dart';
import 'package:truya_pharmis/views/components/custom_form_field.dart';

class RequestRefillPage extends StatelessWidget {
  final Prescription prescription;

  const RequestRefillPage({
    Key key,
    @required this.prescription,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<RequestRefillViewModel>.reactive(
      viewModelBuilder: () => RequestRefillViewModel(),
      onModelReady: (model) => model.initialize(context, prescription),
      builder: (context, model, child) {
        final form = Form(
          key: model.formKey,
          child: Column(
            children: [
              CustomFormField(
                icon: Feather.message_circle,
                labelText: "Comment",
                controller: model.commentCtrl,
                enabled: !model.isLoading,
                maxLines: 3,
                validator: (String value) {
                  if (value.isEmpty || value == null)
                    return "Comment is required.";

                  return null;
                },
              ),
              CustomBtn(
                text: "Request",
                isLoading: model.isLoading,
                onPressed: () {
                  model.save();
                },
              ),
            ],
          ),
        );

        return BaseView(
          givePadding: true,
          isLoading: model.isLoading,
          title: "Request a Refill",
          body: SingleChildScrollView(
            child: form,
          ),
        );
      },
    );
  }
}
