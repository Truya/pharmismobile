import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectycube_sdk/connectycube_sdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:truya_pharmis/constants/firestore_collections.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/views/components/circular_image.dart';
import 'package:truya_pharmis/views/pages/conversation_call/conversation_call.page.dart';
import 'package:im_animations/im_animations.dart';

class IncomingCallPage extends StatelessWidget {
  static const String TAG = "IncomingCallPage";
  final P2PSession callSession;

  const IncomingCallPage({
    Key key,
    @required this.callSession,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    callSession.onSessionClosed = (callSession) {
      log("_onSessionClosed", TAG);
      FlutterRingtonePlayer.stop();
      Navigator.pop(context);
    };

    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: Scaffold(
        // backgroundColor: Colors.black,
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding:
                    EdgeInsets.only(top: 16, bottom: 60, left: 16, right: 16),
                child: Text(
                  _getCallTitle(),
                  style: TextStyle(fontSize: 18),
                ),
              ),
              // Padding(
              //   padding: EdgeInsets.only(top: 36, bottom: 8),
              //   child: Text("Caller", style: TextStyle(fontSize: 20)),
              // ),
              // Padding(
              //   padding: EdgeInsets.only(bottom: 86),
              //   child: Text(
              //     callSession.opponentsIds.join(", "),
              //     style: TextStyle(fontSize: 18),
              //   ),
              // ),
              StreamBuilder(
                stream: userStream(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    QuerySnapshot snap = snapshot.data;
                    DocumentSnapshot doc = snap.docs.first;
                    AppUser appUser = AppUser.fromDoc(doc);
                    return Padding(
                      padding: EdgeInsets.only(bottom: 86),
                      child: Column(
                        children: [
                          ColorSonar(
                            contentAreaRadius: 48.0,
                            waveFall: 15.0,
                            // waveMotionEffect: Curves.elasticIn,
                            waveMotion: WaveMotion.synced,
                            child: CircularImage(
                              image: appUser.photo,
                              size: 80.0,
                            ),
                          ),
                          SizedBox(height: 60.0),
                          Text(
                            appUser.fullname(),
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                  return Text(
                    'loading...',
                    style: TextStyle(fontSize: 18),
                  );
                },
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: 36),
                    child: FloatingActionButton(
                      heroTag: "RejectCall",
                      child: Icon(
                        Icons.call_end,
                        color: Colors.white,
                      ),
                      backgroundColor: Colors.red,
                      onPressed: () => _rejectCall(context, callSession),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 36),
                    child: FloatingActionButton(
                      heroTag: "AcceptCall",
                      child: Icon(
                        Icons.call,
                        color: Colors.white,
                      ),
                      backgroundColor: Colors.green,
                      onPressed: () => _acceptCall(context, callSession),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getCallTitle() {
    String callType;

    switch (callSession.callType) {
      case CallType.VIDEO_CALL:
        callType = "Video";
        break;
      case CallType.AUDIO_CALL:
        callType = "Audio";
        break;
    }

    return "Incoming $callType call";
  }

  void _acceptCall(BuildContext context, P2PSession callSession) {
    FlutterRingtonePlayer.stop();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => ConversationCallPage(callSession, true),
      ),
    );
  }

  void _rejectCall(BuildContext context, P2PSession callSession) {
    callSession.reject();
    FlutterRingtonePlayer.stop();
    // Navigator.pop(context);
  }

  Future<bool> _onBackPressed(BuildContext context) {
    return Future.value(false);
  }

  Stream<QuerySnapshot> userStream() {
    print({
      "Users:": {callSession.callerId}
    });
    return FirebaseFirestore.instance
        .collection(FirestoreCollections.users)
        .where('connectyCubeId', isEqualTo: callSession.callerId)
        .snapshots();
  }
}
