import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/services/bottom_sheet.service.dart';
import 'package:truya_pharmis/view_models/appointment_settings.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/custom_button.dart';
import 'package:truya_pharmis/views/components/custom_form_field.dart';
import 'package:truya_pharmis/views/pages/appointment_settings/widgets/dof_tile.dart';
import 'package:truya_pharmis/views/pages/appointment_settings/widgets/slot_selector.dart';

class AppointmentSettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AppointmentSettingsViewModel>.reactive(
      viewModelBuilder: () => AppointmentSettingsViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        return BaseView(
          isLoading: model.isLoading,
          title: "Appointment Settings",
          actions: [
            IconButton(
              icon: Icon(FlutterIcons.settings_fea),
              onPressed: () {
                BottomSheetService().show(
                  context: context,
                  body: Container(
                    child: Column(
                      children: [
                        SlotSelector(
                          selectedSlot: model.selectedTimePerSlot,
                          slots: model.timeslots,
                          onChanged: model.setTimePerSlot,
                        ),
                        CustomFormField(
                          icon: FlutterIcons.dollar_fou,
                          labelText: "Units charged per slot",
                          controller: model.unitsPerSlotCtrl
                        ),
                        CustomButton(
                          text: "Update",
                          onPressed: () => model.updateSettings(context),
                        ),
                      ],
                    ),
                  ),
                );
              },
            )
          ],
          body: Container(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: ListView.separated(
              itemCount: model.daysOfTheWeek.length,
              itemBuilder: (BuildContext context, int index) {
                return DOWTile(
                  day: model.daysOfTheWeek[index],
                  index: index,
                  available: model.available[index],
                  availability: model.loggedInUser != null
                      ? model
                          .loggedInUser?.availableOn[model.daysOfTheWeek[index]]
                      : null,
                      model: model,
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 10.0,
                );
              },
              padding: EdgeInsets.only(top: 20.0),
            ),
          ),
        );
      },
    );
  }
}
