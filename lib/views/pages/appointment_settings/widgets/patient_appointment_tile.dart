import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/views/components/circular_image.dart';

class PatientAppointmentTile extends StatelessWidget {
  final AppUser user;
  final DateTime startsAt;
  final DateTime endsAt;
  final int index;

  const PatientAppointmentTile({
    Key key,
    this.user,
    this.startsAt,
    this.endsAt,
    this.index,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        leading: Container(
          margin: EdgeInsets.only(right: 5.0),
          child: CircularImage(image: user.photo),
        ),
        title: Text(user.fullname() ?? ""),
        subtitle: Text("2:00PM - 2:30PM"),
      ),
      onPressed: (){},
    );
  }
}
