import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/models/available_at.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/view_models/appointment_settings.vm.dart';
import 'package:truya_pharmis/views/components/custom_button.dart';
import 'package:truya_pharmis/views/components/custom_form_field.dart';
import 'package:truya_pharmis/views/components/switcher_tile.dart';

class ModifyAvailabilityBS extends StatefulWidget {
  final String day;
  final bool available;
  final Map availability;
  final AppointmentSettingsViewModel model;

  const ModifyAvailabilityBS({
    Key key,
    this.day,
    this.available,
    this.availability,
    this.model,
  }) : super(key: key);
  @override
  _ModifyAvailabilityBSState createState() => _ModifyAvailabilityBSState();
}

class _ModifyAvailabilityBSState extends State<ModifyAvailabilityBS> {
  bool _available = false;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  TextEditingController startTimeCtrl = TextEditingController();
  TextEditingController endTimeCtrl = TextEditingController();
  // AvailableAt _availableAt;

  @override
  void initState() {
    setState(() {
      _available = widget.available;
      if (_available) {
        // _availableAt = AvailableAt.fromJson(widget.availableAt);
        // startTimeCtrl.text = _availableAt.startsAt;
        // endTimeCtrl.text = _availableAt.endsAt;
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final form = Visibility(
      visible: _available,
      child: Form(
        key: formKey,
        child: Column(
          children: [
            Theme(
              data: ThemeData(
                primarySwatch: Colors.blue,
              ),
              child: Builder(
                builder: (BuildContext context) {
                  return CustomFormField(
                    icon: FlutterIcons.clock_fea,
                    labelText: "Start Time",
                    readOnly: true,
                    onTap: () async {
                      TimeOfDay time = await showTimePicker(
                        context: context,
                        initialTime: startTimeCtrl.text.length != 0
                            ? Utils.stringToTimeOfDay(startTimeCtrl.text)
                            : TimeOfDay.now(),
                      );

                      if (time != null) {
                        startTimeCtrl.text = Utils.todTo24h(time);
                      }
                    },
                    controller: startTimeCtrl,
                    validator: (String value) {
                      if (_available) {
                        if (value.isEmpty || value == null)
                          return "Start time is required.";
                      }

                      return null;
                    },
                  );
                },
              ),
            ),
            Theme(
              data: ThemeData(
                primarySwatch: Colors.blue,
              ),
              child: Builder(
                builder: (BuildContext context) {
                  return CustomFormField(
                    icon: FlutterIcons.clock_fea,
                    labelText: "End Time",
                    readOnly: true,
                    onTap: () async {
                      TimeOfDay time = await showTimePicker(
                        context: context,
                        initialTime: endTimeCtrl.text.length != 0
                            ? Utils.stringToTimeOfDay(endTimeCtrl.text)
                            : TimeOfDay.now(),
                      );

                      if (time != null) {
                        endTimeCtrl.text = Utils.todTo24h(time);
                      }
                    },
                    controller: endTimeCtrl,
                    validator: (String value) {
                      if (_available) {
                        if (value.isEmpty || value == null)
                          return "End time is required.";
                      }

                      return null;
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
    return Container(
      child: Column(
        children: [
          SwitcherTile(
            title: "Are you available?",
            value: _available,
            onChanged: (bool value) {
              setState(() {
                _available = value;
              });
            },
          ),
          form,
          CustomButton(
            text: "Save",
            onPressed: () {
              if (_available) {
                if (formKey.currentState.validate()) {
                } else {
                  return;
                }
              } else {}
              widget.model.updateAvailability(
                context,
                _available,
                widget.day,
                availableAt: AvailableAt(
                  startsAt: startTimeCtrl.text,
                  endsAt: endTimeCtrl.text,
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
