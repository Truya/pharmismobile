import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/availability.dart';
import 'package:truya_pharmis/models/available_at.dart';
import 'package:truya_pharmis/services/bottom_sheet.service.dart';
import 'package:truya_pharmis/view_models/appointment_settings.vm.dart';
import 'package:truya_pharmis/views/components/bs_option_tile.dart';
import 'package:truya_pharmis/views/components/count_tile.dart';
import 'package:truya_pharmis/views/pages/appointment_settings/widgets/modify_availablity.bs.dart';
import 'package:truya_pharmis/views/pages/appointment_settings/widgets/patient_appointment_tile.dart';
import 'package:velocity_x/velocity_x.dart';

class DOWTile extends StatelessWidget {
  final String day;
  final int index;
  final bool available;
  final Map availability;
  final AppointmentSettingsViewModel model;

  const DOWTile({
    Key key,
    this.day,
    this.index,
    this.available,
    this.availability,
    this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String availabeText() {
      if (availability != null) {
        var text = "";
        Availability _availability = Availability.fromJson(availability);
        List<AvailableAt> hoursAvailable = _availability.hoursAvailable;
        hoursAvailable.forEach((_availableAt) {
          int i = hoursAvailable.indexOf(_availableAt);
          bool isLast = hoursAvailable.indexOf(hoursAvailable[hoursAvailable.length - 1]) == i;
          text += "${_availableAt.title()} ${isLast ? '' : '\n'}";
        });
        return text;
        // return AvailableAt.fromJson(availableAt).title();
      } else {
        return "";
      }
    }

    // String availableText = "2:00PM - 5:00PM";
    String unavailableText = "Unavailable";

    Color availableColor = Theme.of(context).colorScheme.primary;
    Color unavailableColor = Colors.black12.withOpacity(0.05);

    return MaterialButton(
      elevation: available ? 0 : 0,
      color: available ? availableColor : unavailableColor,
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        leading: CountTile(count: index + 1),
        title: Text(day.allWordsCapitilize()),
        subtitle: Text(available ? availabeText() : unavailableText),
        trailing: available
            ? Container(
                height: 20.0,
                width: 20.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).accentColor,
                ),
                child: Center(
                  child: Text(
                    "3",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              )
            : null,
      ),
      onPressed: () {
        BottomSheetService().show(
          showControl: false,
          context: context,
          body: Container(
            child: Column(
              children: [
                Visibility(
                  visible: available,
                  child: BsOptionTile(
                    icon: FlutterIcons.eye_fea,
                    title: "View Appointments",
                    onTap: () {
                      Navigator.pop(context);

                      BottomSheetService().show(
                        showControl: true,
                        context: context,
                        body: Container(
                          height: MediaQuery.of(context).size.height * 0.8,
                          child: ListView(
                            children: [
                              PatientAppointmentTile(
                                user: AppUser(
                                  firstName: "Emmanuel",
                                  lastName: "Fache",
                                  photo:
                                      "https://i.pinimg.com/originals/7c/4b/d8/7c4bd89875c1180dee6f737ff969f908.jpg",
                                ),
                              ),
                              PatientAppointmentTile(
                                user: AppUser(
                                  firstName: "Lord",
                                  lastName: "Nonso",
                                  photo:
                                      "https://i.pinimg.com/originals/7c/4b/d8/7c4bd89875c1180dee6f737ff969f908.jpg",
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
                BsOptionTile(
                  icon: FlutterIcons.calendar_oct,
                  title: "Modify Availability",
                  onTap: () {
                    Navigator.pop(context);

                    BottomSheetService().show(
                      showControl: true,
                      context: context,
                      body: ModifyAvailabilityBS(
                        day: day,
                        available: available,
                        model: model,
                        availability: availability,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
