import 'package:flutter/material.dart';

class SlotSelector extends StatefulWidget {
  final int selectedSlot;
  final List<int> slots;
  final ValueChanged<int> onChanged;

  const SlotSelector({
    Key key,
    this.selectedSlot,
    this.slots,
    this.onChanged,
  }) : super(key: key);

  @override
  _SlotSelectorState createState() => _SlotSelectorState();
}

class _SlotSelectorState extends State<SlotSelector> {
  int selected = 10;
  @override
  void initState() {
    setState(() {
      selected = widget.selectedSlot;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20.0),
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Select time to be spent per slot",
            textAlign: TextAlign.start,
          ),
          Wrap(
            children: widget.slots.map((e) {
              return slotTile(e);
            }).toList(),
            spacing: 25.0,
            runSpacing: 5.0,
            alignment: WrapAlignment.center,
          ),
        ],
      ),
    );
  }

  Widget slotTile(int slot) {
    bool isSelected = selected == slot;
    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      color: isSelected ? Theme.of(context).accentColor : null,
      onPressed: () {
        setState(() {
          selected = slot;
          widget.onChanged(slot);
        });
      },
      child: Text(
        "$slot min",
        style: TextStyle(
          color: isSelected ? Colors.white : null,
        ),
      ),
    );
  }
}
