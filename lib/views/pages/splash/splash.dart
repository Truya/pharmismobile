import 'dart:async';

import 'package:flutter/material.dart';
import 'package:truya_pharmis/constants/images.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/views/pages/auth/login.page.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  startTimeout() {
    return Timer(Duration(milliseconds: 1200), handleTimeout);
  }

  void handleTimeout() {
    changeScreen();
  }

  changeScreen() {
    try {
      Navigate.pushPage(context, LoginPage());
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    startTimeout();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).accentColor,
      body: Center(
        child: Hero(
          tag: 'logo',
          child: Image.asset(
            AppImages.logo,
            width: 150.0,
            height: 150.0,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
