import 'package:flutter/material.dart';
import 'package:truya_pharmis/constants/images.dart';
import 'package:truya_pharmis/services/auth_service.dart';
import 'package:truya_pharmis/views/components/custom_button.dart';

class VerifyEmailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                AppImages.empty,
                // height: imageSize,
                fit: BoxFit.contain,
              ),
              SizedBox(height: 30.0),
              Text(
                'Please Verify your email to continue using the app',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: CustomButton(
                  text: 'Logout',
                  onPressed: () => AuthService().logout(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
