import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/constants/images.dart';
import 'package:truya_pharmis/util/validations.dart';
import 'package:truya_pharmis/view_models/auth_vm.dart';
import 'package:truya_pharmis/views/pages/auth/sign_up_page.dart';
import 'package:truya_pharmis/views/pages/home/home.page.dart';
import 'package:truya_pharmis/views/pages/main_screen.dart';
import 'package:truya_pharmis/widgets/custom_button.dart';
import 'package:truya_pharmis/widgets/custom_text_field.dart';
import 'package:truya_pharmis/widgets/elevated_circle_avatar.dart';

class LoginPage extends StatefulWidget {
  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AuthViewModel>.reactive(
      viewModelBuilder: () => AuthViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        final logo = Align(
          child: Container(
            margin: EdgeInsets.only(top: 50.0),
            child: Hero(
              tag: 'logo',
              child: Image.asset(
                AppImages.logo,
                height: 60.0,
                fit: BoxFit.contain,
              ),
            ),
          ),
        );

        final forgotPassword = Align(
          alignment: Alignment.centerRight,
          child: GestureDetector(
            onTap: () => model.forgotPassword(),
            child: Text(
              'Recover Password?',
              style: TextStyle(
                color: Color(0x99011947),
                fontSize: 11,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        );

        final form = Form(
          key: model.formKey,
          child: Container(
            height: MediaQuery.of(context).size.height / 1.3,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50),
              ),
            ),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      'Log In to your Account',
                      style: TextStyle(
                        fontSize: 24,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomTextField(
                      title: 'EMAIL OR USERNAME',
                      hintText: 'mail@email.com',
                      // controller: controller,
                      validateFunction: Validations.validateString,
                      textInputType: TextInputType.text,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomTextField(
                      title: 'PASSWORD',
                      hintText: '***************',
                      // controller: controller,
                      validateFunction: Validations.validatePassword,
                      textInputType: TextInputType.text,
                      obscureText: true,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    forgotPassword,
                    SizedBox(
                      height: 10,
                    ),
                    CustomButton(
                      label: 'PROCEED',
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return MainScreen();
                            },
                          ),
                        );
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedCircleAvatar(
                      image: AppImages.googlLogo,
                    ),
                    ElevatedCircleAvatar(
                      image: AppImages.facebookLogo,
                    ),
                    ElevatedCircleAvatar(
                      image: AppImages.appleLogo,
                    ),
                  ],
                ),
              ],
            ),
          ),
        );

        return Scaffold(
          backgroundColor: Theme.of(context).accentColor,
          body: SingleChildScrollView(
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  child: Column(
                    children: [
                      logo,
                      SizedBox(
                        height: 20,
                      ),
                      form,
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    height: MediaQuery.of(context).size.height / 5.5,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Theme.of(context).accentColor,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50),
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Icon(
                            Icons.keyboard_arrow_up_outlined,
                            color: Theme.of(context).accentColor,
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'New User? ',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) {
                                      return SignUpPage();
                                    },
                                  ),
                                );
                              },
                              child: Text(
                                'Swipe Up',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
