import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/view_models/call_log.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/infinite_scroll_view_builder.dart';
import 'package:truya_pharmis/views/pages/call_logs/widgets/call_log_tile.dart';

class CallLogsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CallLogsViewModel>.reactive(
      viewModelBuilder: () => CallLogsViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        return BaseView(
          title: "Call Logs",
          body: Container(
            child: InfiniteScrollViewBuilder(
              data: model.callLogs,
              isLoading: model.listLoading,
              onScrollEnd: () => {}, // model.index(),
              onRefresh: () async => model.refresh(),
              itemBuilder: (BuildContext context, int index) {
                return CallLogTile(
                  callLog: model.callLogs[index],
                );
              },
            ),
          ),
        );
      },
    );
  }
}
