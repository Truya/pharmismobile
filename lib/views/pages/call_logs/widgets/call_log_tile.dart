import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/call_log.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:velocity_x/velocity_x.dart';

class CallLogTile extends StatefulWidget {
  final CallLog callLog;

  const CallLogTile({
    Key key,
    this.callLog,
  }) : super(key: key);

  @override
  _CallLogTileState createState() => _CallLogTileState();
}

class _CallLogTileState extends State<CallLogTile> {
  AppUser user;
  AppUser loggedInUser;
  UserRepository userRepo = new UserRepository();

  @override
  void initState() {
    init();
    super.initState();
  }

  void init() async {
    if (userRepo.auth.currentUser.uid == widget.callLog.doctorId) {
      // Dude is a medic
      // So show him patient info
      user = user = AppUser.fromDoc(
        await userRepo.view(widget.callLog.patientId),
      );
    } else {
      // Dude is the patient
      // So show him doctor info
      user = user = AppUser.fromDoc(
        await userRepo.view(widget.callLog.doctorId),
      );
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double size = 42.0;
    Color color = widget.callLog.status == "pending"
        ? Colors.grey
        : widget.callLog.status == "accepted"
            ? Colors.green
            : Colors.red;
    IconData icon = widget.callLog.status == "accepted"
        ? FlutterIcons.phone_outgoing_fea
        : FlutterIcons.phone_missed_fea;
    return user == null
        ? VxSkeleton(
            width: double.infinity,
            height: 70.0,
          )
        : ListTile(
            leading: Container(
              height: size,
              width: size,
              decoration: BoxDecoration(
                color: color.withOpacity(0.3),
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: Icon(
                icon,
              ),
            ),
            title: Text(
              user.fullname(),
              style: TextStyle(fontWeight: FontWeight.w700),
            ),
            subtitle: Text(
              Utils.formatDateTime(widget.callLog.startTime),
            ),
          );
  }
}
