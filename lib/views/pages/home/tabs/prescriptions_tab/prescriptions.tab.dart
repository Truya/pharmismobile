import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/view_models/prescriptions.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/custom_tabbar.dart';
import 'package:truya_pharmis/views/components/infinite_scroll_view_builder.dart';
import 'package:truya_pharmis/views/pages/home/tabs/prescriptions_tab/widgets/prescription_tile.dart';
import 'package:truya_pharmis/views/pages/issue_prescription/issue_prescription.page.dart';

class PrescriptionsTab extends StatefulWidget {
  @override
  _PrescriptionsTabState createState() => _PrescriptionsTabState();
}

class _PrescriptionsTabState extends State<PrescriptionsTab>
    with TickerProviderStateMixin {
  final List<String> _tabs = [
    "Your Prescriptions",
    "Issued Prescriptions",
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      length: _tabs.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<PrescriptionsViewModel>.reactive(
      viewModelBuilder: () => PrescriptionsViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        final tabBar = CustomTabBar(
          tabs: _tabs,
          controller: _tabController,
        );

        final yourPrescriptions = Container(
          child: InfiniteScrollViewBuilder(
            data: model.yourPrescriptions,
            isLoading: model.listLoading,
            onScrollEnd: () => {}, // model.index(),
            onRefresh: () async => model.refresh(),
            itemBuilder: (BuildContext context, int index) {
              return PrescriptionTile(
                prescription: model.yourPrescriptions[index],
                index: index,
              );
            },
          ),
        );

        final issuedPrescriptions = Container(
          child: InfiniteScrollViewBuilder(
            data: model.issuedPrescriptions,
            isLoading: model.listLoading,
            onScrollEnd: () => {}, // model.index(),
            onRefresh: () async => model.refreshIssuedPrescriptions(),
            itemBuilder: (BuildContext context, int index) {
              return PrescriptionTile(
                prescription: model.issuedPrescriptions[index],
                index: index,
              );
            },
          ),
        );

        final tabBarView = Expanded(
          child: TabBarView(
            controller: _tabController,
            children: [yourPrescriptions, issuedPrescriptions],
          ),
        );

        return BaseView(
          appBarElevation: 0,
          isLoading: model.isLoading,
          title: "Prescriptions",
          fab: Visibility(
            visible: model.loggedInUser?.isMedic ?? false,
            child: FloatingActionButton(
              backgroundColor: Theme.of(context).accentColor,
              onPressed: () async {
                AppUser user = await model.bottomSheet.showFindUser(
                  title: "Enter patient email address",
                  context: context,
                );
                if (user != null) {
                  await Navigate.pushPage(
                    context,
                    IssuePrescriptionPage(
                      user: user,
                    ),
                  );
                  model.refresh();
                }
              },
              child: Icon(
                FlutterIcons.plus_fea,
                color: Colors.white,
              ),
            ),
          ),
          body: Container(
            child: Visibility(
              visible: model.loggedInUser?.isMedic ?? false,
              child: Column(
                children: [
                  tabBar,
                  tabBarView,
                ],
              ),
              replacement: yourPrescriptions,
            ),
          ),
        );
      },
    );
  }
}
