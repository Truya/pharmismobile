import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/services/bottom_sheet.service.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/views/components/bs_option_tile.dart';
import 'package:truya_pharmis/views/components/count_tile.dart';
import 'package:truya_pharmis/views/pages/prescription_details/prescription_details.page.dart';
import 'package:truya_pharmis/views/pages/request_refill/request_refill.page.dart';

class PrescriptionTile extends StatelessWidget {
  final Prescription prescription;
  final int index;

  const PrescriptionTile({
    Key key,
    this.prescription,
    this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        leading: CountTile(count: index + 1),
        title: Text(
          Utils.formatDateTime(prescription.issueDate),
        ),
        subtitle: Text(
            "${prescription.medications.length} medication${prescription.medications.length > 1 ? 's' : ''}"),
        trailing: statusBadge(
          prescription.status,
        ),
      ),
      onPressed: () {
        BottomSheetService().show(
          showControl: false,
          context: context,
          body: Container(
            child: Column(
              children: [
                BsOptionTile(
                  icon: FlutterIcons.eye_fea,
                  title: "View Details",
                  onTap: () {
                    Navigator.pop(context);
                    Navigate.pushPage(
                      context,
                      PrescriptionDetailsPage(
                        prescription: prescription,
                      ),
                    );
                  },
                ),
                BsOptionTile(
                  icon: FlutterIcons.refresh_cw_fea,
                  title: "Request a Refill",
                  onTap: () {
                    Navigator.pop(context);
                    Navigate.pushPage(
                      context,
                      RequestRefillPage(prescription: prescription),
                    );
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget statusBadge(status) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
      decoration: BoxDecoration(
        color: status == "ISSUED"
            ? Colors.grey
            : status == "FULFILLED"
                ? Colors.green
                : Colors.red,
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Text(
        status,
        style: TextStyle(fontSize: 12.0, color: Colors.white),
      ),
    );
  }
}
