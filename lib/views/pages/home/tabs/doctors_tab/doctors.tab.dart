import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/view_models/doctors.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/infinite_scroll_view_builder.dart';

import 'widgets/doctor_tile.dart';

class DoctorsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<DoctorsViewModel>.reactive(
      viewModelBuilder: () => DoctorsViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        return BaseView(
          isLoading: model.isLoading,
          title: "Doctors",
          body: Container(
            child: InfiniteScrollViewBuilder(
              data: model.doctors,
              isLoading: model.listLoading,
              onScrollEnd: () => {}, // model.index(),
              onRefresh: () async => model.refresh(),
              itemBuilder: (BuildContext context, int index) {
                return DoctorTile(
                  user: model.doctors[index],
                );
              },
            ),
          ),
        );
      },
    );
  }
}
