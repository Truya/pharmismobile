import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/slot.dart';

class SlotTile extends StatefulWidget {
  final Slot slot;
  final ValueChanged<Slot> onSelected;
  final ValueChanged<Slot> onDisSelected;
  final bool selected;

  const SlotTile({
    Key key,
    this.slot,
    this.onSelected,
    this.onDisSelected,
    this.selected = false,
  }) : super(key: key);

  @override
  _SlotTileState createState() => _SlotTileState();
}

class _SlotTileState extends State<SlotTile> {
  bool selected = false;
  @override
  void initState() {
    setState(() {
      selected = widget.selected;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Color color = widget.slot.booked == true ? Colors.red : Colors.green;
    String booked = widget.slot.booked ? "Booked" : "Open";
    String title =
        widget.slot.startsAt + " - " + widget.slot.endsAt + " ($booked)";
    return Container(
      decoration: BoxDecoration(
        color: selected ? color : null,
        border: Border(
          left: BorderSide(
            color: color,
            width: 3.0,
          ),
          bottom: BorderSide(
            color: color,
            width: 2.0,
          ),
        ),
      ),
      margin: const EdgeInsets.symmetric(
        horizontal: 8.0,
        vertical: 4.0,
      ),
      child: ListTile(
        title: Text(
          title,
          style: TextStyle(
            color: selected ? Colors.white : null,
          ),
        ),
        onTap: () {
          setState(() {
            if (!widget.slot.booked) {
              selected = !selected;
              if (selected) {
                widget.onSelected(widget.slot);
              } else {
                widget.onDisSelected(widget.slot);
              }
            }
          });
        },
      ),
    );
  }
}
