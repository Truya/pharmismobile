import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/slot.dart';
import 'package:truya_pharmis/view_models/book_appointment.vm.dart';
import 'package:truya_pharmis/views/components/bs_button.dart';
import 'package:truya_pharmis/views/pages/home/tabs/doctors_tab/widgets/slot_tile.dart';

class BookAppointmentBS extends StatefulWidget {
  final AppUser doctor;

  const BookAppointmentBS({
    Key key,
    @required this.doctor,
  }) : super(key: key);
  @override
  _BookAppointmentBSState createState() => _BookAppointmentBSState();
}

class _BookAppointmentBSState extends State<BookAppointmentBS>
    with TickerProviderStateMixin {
  CalendarController _calendarController;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<BookAppointmentViewModel>.reactive(
      viewModelBuilder: () => BookAppointmentViewModel(),
      disposeViewModel: false,
      onModelReady: (model) => model.initialize(context, widget.doctor),
      builder: (context, model, child) {
        final calendar = Container(
          child: TableCalendar(
            calendarController: _calendarController,
            startDay: DateTime.now(),
            endDay: DateTime.now().add(Duration(days: 14)),
            events: model.events,
            calendarStyle: CalendarStyle(
              outsideDaysVisible: false,
              unavailableStyle: TextStyle(
                color: Colors.grey.withOpacity(0.3),
              ),
              weekendStyle: TextStyle(color: Colors.black87),
              weekdayStyle: TextStyle(color: Colors.black87),
            ),
            initialCalendarFormat: CalendarFormat.twoWeeks,
            onDaySelected: (DateTime day, List events, List holidays) {
              model.setAvailableSlots(events);
            },
          ),
        );

        return Container(
          height: MediaQuery.of(context).size.height * .8,
          child: Column(
            children: [
              calendar,
              Expanded(
                child: ListView.builder(
                  itemCount: model.selectedEvents.length,
                  itemBuilder: (BuildContext context, int i) {
                    return SlotTile(
                      slot: model.selectedEvents[i],
                      onSelected: (Slot slot) =>  model.updateUserSelectedSlots(slot, true),
                      onDisSelected: (Slot slot) =>  model.updateUserSelectedSlots(slot, false),
                    );
                  },
                ),
              ),
              BsButton(
                title: "Book Appointment".toUpperCase(),
                onTap: () => model.bookAppointment(context)
              )
            ],
          ),
        );
      },
    );
  }
}
