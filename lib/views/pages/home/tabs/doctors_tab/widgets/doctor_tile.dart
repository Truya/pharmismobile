import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/services/bottom_sheet.service.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/views/components/bs_option_tile.dart';
import 'package:truya_pharmis/views/components/circular_image.dart';
import 'package:truya_pharmis/views/pages/user_details/user_details.page..dart';

import 'book_appointment.bs.dart';

class DoctorTile extends StatelessWidget {
  final AppUser user;

  const DoctorTile({
    Key key,
    this.user,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: ListTile(
        contentPadding: EdgeInsets.zero,
        leading: Container(
          margin: EdgeInsets.only(right: 5.0),
          child: CircularImage(image: user.photo),
        ),
        title: Text(user.fullname() ?? ""),
        subtitle: Text(
          "Lagos Specialist Hospital",
        ),
      ),
      onPressed: () {
        BottomSheetService().show(
          showControl: false,
          context: context,
          body: Container(
            child: Column(
              children: [
                BsOptionTile(
                  icon: FlutterIcons.eye_fea,
                  title: "View Details",
                  onTap: () {
                    Navigator.pop(context);
                    Navigate.pushPage(
                      context,
                      UserDetailsPage(
                        user: user,
                      ),
                    );
                  },
                ),
                BsOptionTile(
                  icon: FlutterIcons.calendar_fea,
                  title: "Book Appointment",
                  onTap: () async {
                    Navigator.pop(context);
                    BottomSheetService().show(
                      context: context,
                      title: "Choose Date",
                      padding: EdgeInsets.only(top: 20.0),
                      body: BookAppointmentBS(
                        doctor: user,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
