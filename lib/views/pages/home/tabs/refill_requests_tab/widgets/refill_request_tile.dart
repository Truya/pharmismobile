import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/models/refill_request.dart';
import 'package:truya_pharmis/repositories/prescription.repository.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'package:truya_pharmis/services/bottom_sheet.service.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/view_models/refill_requests.vm.dart';
import 'package:truya_pharmis/views/components/bs_option_tile.dart';
import 'package:truya_pharmis/views/components/circular_image.dart';
import 'package:truya_pharmis/views/components/custom_btn.dart';
import 'package:truya_pharmis/views/pages/prescription_details/prescription_details.page.dart';
import 'package:truya_pharmis/views/pages/refill_request_details/refill_request_details.page.dart';
import 'package:velocity_x/velocity_x.dart';

class RefillRequestTile extends StatefulWidget {
  final RefillRequest request;
  final int index;
  final RefillRequestsViewModel model;
  final bool viewingAsPatient;

  const RefillRequestTile({
    Key key,
    this.request,
    this.index,
    this.model,
    this.viewingAsPatient = false,
  }) : super(key: key);

  @override
  _RefillRequestTileState createState() => _RefillRequestTileState();
}

class _RefillRequestTileState extends State<RefillRequestTile> {
  AppUser user;
  AppUser loggedInUser;
  UserRepository userRepo = new UserRepository();

  @override
  void initState() {
    init();
    super.initState();
  }

  void init() async {
    // loggedInUser = AppUser.fromDoc(
    //   await userRepo.view(userRepo.auth.currentUser.uid),
    // );
    if (userRepo.auth.currentUser.uid == widget.request.issuerId) {
      // Dude is a medic
      user = user = AppUser.fromDoc(
        await userRepo.view(widget.request.issuerId),
      );
    } else {
      // Dude is the patient
      user = user = AppUser.fromDoc(
        await userRepo.view(widget.request.userId),
      );
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return user == null
        ? VxSkeleton(
            width: double.infinity,
            height: 70.0,
          )
        : MaterialButton(
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              // leading: CountTile(count: widget.index + 1),
              leading: Container(
                margin: EdgeInsets.only(right: 5.0),
                child: CircularImage(image: user.photo),
              ),
              title: Text(user.fullname() ?? ""),
              subtitle: Text(
                Utils.formatDateTime(widget.request.requestDate),
              ),
              trailing: statusBadge(
                widget.request.status,
              ),
            ),
            onPressed: () {
              BottomSheetService().show(
                showControl: false,
                context: context,
                body: Container(
                  child: Column(
                    children: [
                      BsOptionTile(
                        icon: FlutterIcons.eye_fea,
                        title: "View Details",
                        onTap: () {
                          Navigator.pop(context);
                          Navigate.pushPage(
                            context,
                            RefillRequestDetailsPage(
                              request: widget.request,
                              index: widget.index,
                              vm: widget.model,
                            ),
                          );
                        },
                      ),
                      BsOptionTile(
                        icon: FlutterIcons.prescription_mco,
                        title: "View Prescription Details",
                        onTap: () async {
                          Navigator.pop(context);
                          var prescription = Prescription.fromDoc(
                            await PrescriptionRepository().view(
                              widget.request.prescriptionId,
                            ),
                          );
                          Navigate.pushPage(
                            context,
                            PrescriptionDetailsPage(
                              prescription: prescription,
                            ),
                          );
                        },
                      ),
                      Visibility(
                        visible: widget.model.loggedInUser?.isMedic ?? false,
                        child: Visibility(
                          visible: widget.request.status == "PENDING" && widget.request.issuerId == widget.model.loggedInUser.id,
                          child: BsOptionTile(
                            icon: FlutterIcons.menu_open_mco,
                            title: "Actions",
                            onTap: () {
                              Navigator.pop(context);

                              BottomSheetService().show(
                                showControl: false,
                                context: context,
                                body: Container(
                                  child: Column(
                                    children: [
                                      CustomBtn(
                                        text: "Approve",
                                        isOutline: true,
                                        color: Colors.green,
                                        textColor: Colors.green,
                                        onPressed: () async {
                                          await widget.model.changeStatus(
                                            widget.request.id,
                                            "APPROVED",
                                            widget.index,
                                          );
                                          Navigator.pop(context);
                                        },
                                      ),
                                      CustomBtn(
                                        text: "Reject",
                                        color: Colors.red,
                                        onPressed: () async {
                                          await widget.model.changeStatus(
                                            widget.request.id,
                                            "CANCELLED",
                                            widget.index,
                                          );
                                          Navigator.pop(context);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
  }

  Widget statusBadge(status) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
      decoration: BoxDecoration(
        color: status == "PENDING"
            ? Colors.grey
            : status == "APPROVED"
                ? Colors.green
                : Colors.red,
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Text(
        status,
        style: TextStyle(fontSize: 12.0, color: Colors.white),
      ),
    );
  }
}
