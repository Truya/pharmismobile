import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/services/bottom_sheet.service.dart';
import 'package:truya_pharmis/view_models/refill_requests.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/custom_btn.dart';
import 'package:truya_pharmis/views/components/custom_tabbar.dart';
import 'package:truya_pharmis/views/components/infinite_scroll_view_builder.dart';
import 'package:truya_pharmis/views/components/status_selector.dart';
import 'package:truya_pharmis/views/pages/home/tabs/refill_requests_tab/widgets/refill_request_tile.dart';

class RefillRequestsTab extends StatefulWidget {
  @override
  _RefillRequestsTabState createState() => _RefillRequestsTabState();
}

class _RefillRequestsTabState extends State<RefillRequestsTab>
    with TickerProviderStateMixin {
  final List<String> _tabs = [
    "Patients Requests",
    "Your Requests",
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      length: _tabs.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<RefillRequestsViewModel>.reactive(
      viewModelBuilder: () => RefillRequestsViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        final tabBar = CustomTabBar(
          tabs: _tabs,
          controller: _tabController,
        );

        final yourRequests = Container(
          child: InfiniteScrollViewBuilder(
            data: model.requests,
            isLoading: model.listLoading,
            onScrollEnd: () => {}, // model.index(),
            onRefresh: () async => model.yourRequestRefresh(),
            itemBuilder: (BuildContext context, int index) {
              return RefillRequestTile(
                request: model.requests[index],
                index: index,
                model: model,
                viewingAsPatient: true,
              );
            },
          ),
        );

        final patientsRequests = Container(
          child: InfiniteScrollViewBuilder(
            data: model.patientsRequests,
            isLoading: model.listLoading,
            onScrollEnd: () => {}, // model.index(),
            onRefresh: () async => model.patientsRequestRefresh(),
            itemBuilder: (BuildContext context, int index) {
              return RefillRequestTile(
                request: model.patientsRequests[index],
                index: index,
                model: model,
              );
            },
          ),
        );

        final tabBarView = Expanded(
          child: TabBarView(
            controller: _tabController,
            children: [patientsRequests, yourRequests],
          ),
        );

        return BaseView(
          appBarElevation: 0,
          isLoading: model.isLoading,
          title: "Refill Requests",
          actions: [
            IconButton(
              icon: Icon(
                Icons.filter_list,
              ),
              onPressed: () {
                BottomSheetService().show(
                  title: "Select Status",
                  context: context,
                  body: Container(
                    child: Column(
                      children: [
                        StatusSelector(
                          statusList: model.statusList,
                          selectedStatus: model.selectedStatus,
                          onChanged: model.onStatusChanged,
                        ),
                        CustomBtn(
                          text: "Filter",
                          onPressed: () {
                            Navigator.pop(context);
                            print(_tabController.index);
                            model.refreshOne(_tabController.index == 0 ? true : false);
                          },
                        )
                      ],
                    ),
                  ),
                );
              },
            )
          ],
          body: Container(
            child: Visibility(
              visible: model.loggedInUser?.isMedic ?? false,
              child: Column(
                children: [
                  tabBar,
                  tabBarView,
                ],
              ),
              replacement: yourRequests,
            ),
          ),
        );
      },
    );
  }
}
