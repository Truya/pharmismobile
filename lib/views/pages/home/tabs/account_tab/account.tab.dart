import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/services/auth_service.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/view_models/account.vm.dart';
import 'package:truya_pharmis/views/pages/appointments/appointments.page.dart';
import 'package:truya_pharmis/views/pages/call_logs/call_logs.page.dart';
import 'package:truya_pharmis/views/pages/settings/settings.page.dart';
import 'package:velocity_x/velocity_x.dart';

import 'widgets/option_tile.dart';
import 'widgets/user_details.dart';

class AccountTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AccountViewModel>.reactive(
      viewModelBuilder: () => AccountViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        final userDetails = StreamBuilder(
          stream: model.currentUserStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return UserDetails(user: AppUser.fromDoc(snapshot.data));
            } else {
              return Padding(
                padding: EdgeInsets.all(35.0),
                child: VxSkeleton(),
              );
            }
          },
        );

        final options = Container(
          margin: EdgeInsets.only(top: 15.0),
          child: Column(
            children: <Widget>[
              OptionTile(
                icon: FlutterIcons.credit_card_fea,
                iconColor: Colors.green,
                title: "Payments",
                onTap: () {},
              ),
              OptionTile(
                icon: FlutterIcons.calendar_fea,
                iconColor: Colors.teal,
                title: "Appointments",
                onTap: () {
                  Navigate.pushPage(
                    context,
                    AppointmentsPage(),
                  );
                },
              ),
              OptionTile(
                icon: FlutterIcons.phone_call_fea,
                iconColor: Colors.red,
                title: "Call Logs",
                onTap: () {
                  Navigate.pushPage(
                    context,
                    CallLogsPage(),
                  );
                },
              ),
              OptionTile(
                icon: FlutterIcons.settings_fea,
                iconColor: Colors.purple,
                title: "Settings",
                onTap: () {
                  Navigate.pushPage(
                    context,
                    SettingsPage(),
                  );
                },
              ),
              OptionTile(
                icon: FlutterIcons.lock_fea,
                iconColor: Colors.blue,
                title: "Reset Password",
                onTap: () {},
              ),
              OptionTile(
                icon: FlutterIcons.log_out_fea,
                iconColor: Colors.grey,
                title: "Logout",
                onTap: () async {
                  await AuthService().logout();
                },
              ),
            ],
          ),
        );

        final deletAccount = Container(
          padding: EdgeInsets.only(top: 50.0, left: 20.0, right: 20.0),
          child: OutlineButton(
            borderSide: BorderSide(
              color: Colors.red,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            onPressed: () {},
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    FlutterIcons.trash_2_fea,
                    color: Colors.red,
                    size: 17.0,
                  ),
                  Utils.horizontalSpacer(space: 4),
                  Text(
                    "DELETE ACCOUNT",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

        return Container(
          width: double.infinity,
          child: Column(
            children: [userDetails, options, deletAccount],
          ),
        );
      },
    );
  }
}
