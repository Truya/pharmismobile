import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/views/components/circular_image.dart';
import 'package:truya_pharmis/views/components/header_text.dart';
import 'package:truya_pharmis/views/pages/profile/edit_profile.dart';

class UserDetails extends StatelessWidget {
  final AppUser user;

  const UserDetails({
    Key key,
    this.user,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final userImage = CircularImage(
      image: user.photo,
      size: 100,
    );

    final userName = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          HeaderText(
            text: user.fullname(),
            fontSize: 20,
          ),
          Utils.horizontalSpacer(),
          HeaderText(
            text: user.email,
            isBig: false,
          ),
          OutlineButton(
            borderSide: BorderSide(
              color: Theme.of(context).colorScheme.onPrimary,
            ),
            onPressed: () => Navigate.pushPage(context, EditProfile()),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
            child: Text(
              "Edit",
              style: TextStyle(
                color: Theme.of(context).colorScheme.onPrimary,
              ),
            ),
          )
        ],
      ),
    );

    final userDetails = Container(
      color: Colors.grey.withOpacity(0.1),
      padding: EdgeInsets.symmetric(
        horizontal: 20.0,
        vertical: 20.0,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          userImage,
          Utils.horizontalSpacer(),
          userName,
        ],
      ),
    );

    return userDetails;
  }
}
