import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OptionTile extends StatelessWidget {
  final IconData icon;
  final Color iconColor;
  final String title;
  final VoidCallback onTap;

  const OptionTile({
    Key key,
    @required this.icon,
    @required this.iconColor,
    @required this.title,
    this.onTap,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = 35.0;
    return ListTile(
      onTap: onTap,
      title: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.w600,
          color: Theme.of(context).textTheme.bodyText1.color.withOpacity(0.7)
        ),
      ),
      leading: Container(
        height: size,
        width: size,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: iconColor,
        ),
        child: Icon(
          icon,
          color: Colors.white,
          size: 20,
        ),
      ),
      trailing: Icon(CupertinoIcons.right_chevron),
    );
  }
}
