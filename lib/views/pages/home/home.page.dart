import 'package:connectycube_sdk/connectycube_sdk.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'package:truya_pharmis/views/pages/home/tabs/account_tab/account.tab.dart';
import 'package:truya_pharmis/views/pages/home/tabs/doctors_tab/doctors.tab.dart';
import 'package:truya_pharmis/views/pages/home/tabs/prescriptions_tab/prescriptions.tab.dart';
import 'package:truya_pharmis/views/pages/home/tabs/refill_requests_tab/refill_requests.tab.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:truya_pharmis/views/pages/incoming_call/incoming_call.page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  P2PClient _callClient = P2PClient.instance;
  P2PSession _currentCall;

  final List<Widget> _pages = [
    DoctorsTab(),
    PrescriptionsTab(),
    RefillRequestsTab(),
    AccountTab(),
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  // Connectycube

  void _initCalls() {
    _callClient = P2PClient.instance;
    _callClient.init();

    _callClient.onReceiveNewSession = (callSession) {
      if (_currentCall != null &&
          _currentCall.sessionId != callSession.sessionId) {
        callSession.reject();
        return;
      }

      FlutterRingtonePlayer.play(
        android: AndroidSounds.ringtone,
        ios: IosSounds.triTone,
        looping: true,
        asAlarm: false,
      );
      _showIncomingCallScreen(callSession);
    };

    _callClient.onSessionClosed = (callSession) {
      if (_currentCall != null &&
          _currentCall.sessionId == callSession.sessionId) {
        _currentCall = null;
      }
    };
  }

  void _showIncomingCallScreen(P2PSession callSession) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => IncomingCallPage(
          callSession: callSession,
        ),
      ),
    );
  }

  void _initCustomMediaConfigs() {
    RTCMediaConfig mediaConfig = RTCMediaConfig.instance;
    mediaConfig.minHeight = 720;
    mediaConfig.minWidth = 1280;
    mediaConfig.minFrameRate = 30;
  }

  void _initConnectycube() async {
    User user = FirebaseAuth.instance.currentUser;
    AppUser appUser = AppUser.fromDoc(
      await UserRepository().view(user.uid),
    );

    CubeUser cUser = CubeUser(
      login: appUser.email,
      email: appUser.email,
      fullName: appUser.fullname(),
      password: appUser.id,
    );

    if (appUser.connectyCubeId == null) {
      createSession();
      // Create account
      CubeUser nUser = await signUp(cUser);
      await UserRepository().update(user.uid, {"connectyCubeId": nUser.id});
    }

    if (!CubeSessionManager.instance.isActiveSessionValid()) {
      // If no valid active session
      // Create Session
      await createSession(
        CubeUser(
          id: appUser.connectyCubeId,
          login: appUser.email,
          password: user.uid,
        ),
      );
    }

    await CubeChatConnection.instance.login(
      CubeUser(
        id: appUser.connectyCubeId,
        login: appUser.email,
        password: user.uid,
      ),
    );
    _initCustomMediaConfigs();
    _initCalls();
  }

  // End Connectycube

  @override
  void initState() {
    _initConnectycube();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bottomNavBar = BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      onTap: onTabTapped,
      currentIndex: _currentIndex,
      selectedItemColor: Theme.of(context).accentColor,
      unselectedItemColor: Colors.grey.withOpacity(0.6),
      elevation: 4.0,
      showUnselectedLabels: true,
      items: [
        bottomNavItem(
          icon: Icons.supervised_user_circle_outlined,
          title: 'Doctors',
          activeIcon: Icons.supervised_user_circle,
        ),
        bottomNavItem(
          icon: FlutterIcons.prescription_mco,
          title: 'Prescriptions',
        ),
        // bottomNavItem(icon: FlutterIcons.history_mco, title: 'History'),
        bottomNavItem(
          icon: FlutterIcons.refresh_ccw_fea,
          title: 'Refill Req..',
        ),
        bottomNavItem(icon: FlutterIcons.user_fea, title: 'Account'),
      ],
    );

    return Scaffold(
      body: SafeArea(
        child: _pages[_currentIndex],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.4),
              blurRadius: 5,
            )
          ],
        ),
        child: bottomNavBar,
      ),
    );
  }

  BottomNavigationBarItem bottomNavItem({
    @required IconData icon,
    @required String title,
    IconData activeIcon,
  }) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      activeIcon: activeIcon != null ? Icon(activeIcon) : null,
      label: title,
    );
  }
}
