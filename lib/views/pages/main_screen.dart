import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:truya_pharmis/providers/BottomBarProvider.dart';
import 'package:truya_pharmis/views/pages/prescriptions_screen/prescription_screen.dart';
import 'package:truya_pharmis/views/pages/profile_screen/profile_screen.dart';
import 'package:truya_pharmis/views/pages/refill_request_screen/refill_request_screen.dart';
import 'package:truya_pharmis/widgets/bottom_bar.dart';

import 'doctor_screen/doctor_screen.dart';
import 'home_screen/home_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with AutomaticKeepAliveClientMixin<MainScreen> {
  String uid;
  PageController _pageController = PageController();
  bool loading = true;

  @override
  bool get wantKeepAlive => true;
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: PageView(
        onPageChanged: (index) {
          Provider.of<BottomBarProvider>(context, listen: false)
              .changeSelectedItem(index);
        },
        children: [
          HomeScreen(),
          DoctorScreen(),
          PrescriptionsScreen(),
          RefillRequestScreen(),
          ProfileScreen(),
        ],
        controller: _pageController,
      ),
      bottomNavigationBar: Consumer<BottomBarProvider>(
        builder: (context, bottom, _) {
          return BottomBar(
            currentIndex: bottom.selectedItem,
            onTap: (index) {
              Provider.of<BottomBarProvider>(context, listen: false)
                  .changeSelectedItem(index);

              _pageController.animateToPage(bottom.selectedItem,
                  duration: Duration(milliseconds: 200), curve: Curves.linear);
            },
          );
        },
      ),
    );
  }
}
