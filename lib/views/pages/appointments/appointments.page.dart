import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/view_models/appointments.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/custom_tabbar.dart';
import 'package:truya_pharmis/views/components/infinite_scroll_view_builder.dart';
import 'package:truya_pharmis/views/pages/appointment_settings/appointment_settings.page.dart';
import 'package:truya_pharmis/views/pages/appointments/widgets/appointment_tile.dart';

class AppointmentsPage extends StatefulWidget {
  @override
  _AppointmentsPageState createState() => _AppointmentsPageState();
}

class _AppointmentsPageState extends State<AppointmentsPage>
    with TickerProviderStateMixin {
  final List<String> _tabs = [
    "Patients Appointments",
    "Your Appointments",
  ];

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      length: _tabs.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AppointmentsViewModel>.reactive(
      viewModelBuilder: () => AppointmentsViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        final tabBar = CustomTabBar(
          tabs: _tabs,
          controller: _tabController,
        );

        final yourAppointments = Container(
          child: InfiniteScrollViewBuilder(
            data: model.yourAppointments,
            isLoading: model.listLoading,
            onScrollEnd: () => {}, // model.index(),
            onRefresh: () async => model.yourAppointmentsRefresh(),
            itemBuilder: (BuildContext context, int index) {
              return AppointmentTile(
                appointment: model.yourAppointments[index],
                viewingAsPatient: true,
              );
            },
          ),
        );

        final patientsAppointments = Container(
          child: InfiniteScrollViewBuilder(
            data: model.patientAppointments,
            isLoading: model.listLoading,
            onScrollEnd: () => {}, // model.index(),
            onRefresh: () async => model.patientsAppointmentsRefresh(),
            itemBuilder: (BuildContext context, int index) {
              return AppointmentTile(
                appointment: model.patientAppointments[index],
                viewingAsPatient: false,
              );
            },
          ),
        );

        final tabBarView = Expanded(
          child: TabBarView(
            controller: _tabController,
            children: [patientsAppointments, yourAppointments],
          ),
        );

        return BaseView(
          appBarElevation: 0,
          isLoading: model.isLoading,
          title: "Appointments",
          actions: [
            Visibility(
              visible: model.loggedInUser?.isMedic ?? false,
              child: IconButton(
                icon: Icon(FlutterIcons.settings_fea),
                onPressed: () {
                  Navigate.pushPage(
                    context,
                    AppointmentSettingsPage(),
                  );
                },
              ),
            )
          ],
          body: Container(
            child: Visibility(
              visible: model.loggedInUser?.isMedic ?? false,
              child: Column(
                children: [
                  tabBar,
                  tabBarView,
                ],
              ),
              replacement: yourAppointments,
            ),
          ),
        );
      },
    );
  }
}
