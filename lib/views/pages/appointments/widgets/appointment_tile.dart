import 'package:connectycube_sdk/connectycube_sdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/appointment.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'package:truya_pharmis/services/bottom_sheet.service.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/views/components/bs_option_tile.dart';
import 'package:truya_pharmis/views/components/date_tile.dart';
import 'package:truya_pharmis/views/pages/conversation_call/conversation_call.page.dart';
import 'package:velocity_x/velocity_x.dart';

class AppointmentTile extends StatefulWidget {
  final Appointment appointment;
  final bool viewingAsPatient;

  const AppointmentTile({
    Key key,
    this.appointment,
    this.viewingAsPatient = true,
  }) : super(key: key);

  @override
  _AppointmentTileState createState() => _AppointmentTileState();
}

class _AppointmentTileState extends State<AppointmentTile> {
  AppUser user;
  AppUser loggedInUser;
  UserRepository userRepo = new UserRepository();

  @override
  void initState() {
    init();
    super.initState();
  }

  void init() async {
    // loggedInUser = AppUser.fromDoc(
    //   await userRepo.view(userRepo.auth.currentUser.uid),
    // );
    if (userRepo.auth.currentUser.uid == widget.appointment.doctorId) {
      // Dude is a medic
      // So show him patient info
      user = user = AppUser.fromDoc(
        await userRepo.view(widget.appointment.patientId),
      );
    } else {
      // Dude is the patient
      // So show him doctor info
      user = user = AppUser.fromDoc(
        await userRepo.view(widget.appointment.doctorId),
      );
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return user == null
        ? VxSkeleton(
            width: double.infinity,
            height: 70.0,
          )
        : MaterialButton(
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              leading: Container(
                margin: EdgeInsets.only(right: 5.0),
                child: DateTile(
                  date: widget.appointment.startsAt,
                ),
              ),
              title: Text(
                user.fullname() ?? "",
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
              subtitle: Row(
                children: [
                  Icon(
                    FlutterIcons.clock_fea,
                    color: Colors.grey,
                    size: 15,
                  ),
                  Utils.horizontalSpacer(space: 5),
                  Text(
                    Utils.getOnlyTime(
                      widget.appointment.startsAt,
                    ),
                  ),
                ],
              ),
              trailing: statusBadge(
                widget.appointment.getStatus(),
              ),
            ),
            onPressed: () {
              BottomSheetService().show(
                showControl: false,
                context: context,
                body: Container(
                  child: Column(
                    children: [
                      BsOptionTile(
                        icon: FlutterIcons.eye_fea,
                        title: "View Details",
                        onTap: () {
                          Navigator.pop(context);
                          // Navigate.pushPage(
                          //   context,
                          //   PrescriptionDetailsPage(
                          //     prescription: prescription,
                          //   ),
                          // );
                        },
                      ),
                      BsOptionTile(
                        icon: FlutterIcons.video_fea,
                        title: "Start Appointment",
                        onTap: () {
                          Navigator.pop(context);
                          _startCall(
                            CallType.VIDEO_CALL,
                            [user.connectyCubeId].toSet(),
                          );
                        },
                      ),
                      Visibility(
                        visible: !widget.appointment.isCancelled(),
                        child: BsOptionTile(
                          icon: FlutterIcons.stop_circle_fea,
                          title: "Cancel",
                          onTap: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
  }

  Widget statusBadge(status) {
    String statusToShow = status;
    bool isMedic = loggedInUser?.isMedic ?? false;
    if (widget.appointment.status == "DOCTOR_CANCELLED" && isMedic ||
        widget.appointment.status == "PATIENT_CANCELLED" && !isMedic) {
      statusToShow = "CANCELLED BY YOU";
    }
    return Container(
      padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 8.0),
      decoration: BoxDecoration(
        color: status == "PENDING"
            ? Colors.grey
            : status == "COMPLETED"
                ? Colors.green
                : Colors.red,
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Text(
        statusToShow,
        style: TextStyle(fontSize: 11.0, color: Colors.white),
      ),
    );
  }

  void _startCall(int callType, Set<int> opponents) {
    if (opponents.isEmpty) return;

    P2PClient _callClient = P2PClient.instance;

    P2PSession callSession = _callClient.createCallSession(callType, opponents);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ConversationCallPage(callSession, false),
      ),
    );
  }
}
