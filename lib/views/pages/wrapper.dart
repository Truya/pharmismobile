import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:truya_pharmis/constants/firebase.dart';
import 'package:truya_pharmis/views/pages/auth/verify_email.page.dart';
import 'package:truya_pharmis/views/pages/home/home.page.dart';
import 'package:truya_pharmis/views/pages/splash/splash.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: firebaseAuth.authStateChanges(),
      builder: (BuildContext context, AsyncSnapshot<User> snapshot) {
        if (snapshot.hasData) {
          User user = snapshot.data;
          if (user.emailVerified) {
            return HomePage();
          } else {
            return VerifyEmailPage();
          }
        }
        return Splash();
      },
    );
  }
}
