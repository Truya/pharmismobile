import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/tileController.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/view_models/prescription_details.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/custom_row.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';

import 'widgets/medication_tile.dart';

class PrescriptionDetailsPage extends StatelessWidget {
  final Prescription prescription;

  const PrescriptionDetailsPage({
    Key key,
    this.prescription,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<PrescriptionDetailsViewModel>.reactive(
      viewModelBuilder: () => PrescriptionDetailsViewModel(),
      onModelReady: (model) => model.initialize(context, prescription),
      builder: (context, model, child) {
        final contentBg = Colors.grey.withOpacity(0.05);
        final iconColor = Colors.black87;
        final headerTextStyle = TextStyle(
          color: Colors.black87,
          fontWeight: FontWeight.w600
        );

        final basicDetails = Container(
          child: ExpandedTile(
            controller: ExpandedTileController(),
            leading: Icon(Icons.info_outline, color: iconColor),
            centerHeaderTitle: false,
            title: Text("Basic Info", style: headerTextStyle),
            content: Column(
              children: <Widget>[
                CustomRow(
                  title: "Description",
                  value: prescription.description,
                ),
                CustomRow(
                  title: "Notes",
                  value: prescription.notes,
                ),
                CustomRow(
                  title: "Issue Date",
                  value: Utils.formatDateTime(prescription.issueDate),
                ),
                CustomRow(
                  title: "Status",
                  value: prescription.status,
                ),
              ],
            ),
            contentBackgroundColor: contentBg,
          ),
        );

        final medicationDetails = Container(
          child: ExpandedTile(
            controller: ExpandedTileController(),
            leading: Icon(FlutterIcons.medicinebox_ant, color: iconColor),
            centerHeaderTitle: false,
            title: Text("Medications (${prescription.medications.length})", style: headerTextStyle),
            content: Container(
              child: Column(
                children: _buildMedications(),
              ),
            ),
            contentBackgroundColor: contentBg,
          ),
        );

        final organizationDetails = Container(
          child: ExpandedTile(
            controller: ExpandedTileController(),
            leading: Icon(FlutterIcons.office_building_mco, color: iconColor),
            centerHeaderTitle: false,
            title: Text("Organization Details", style: headerTextStyle),
            content: Column(
              children: <Widget>[
                CustomRow(
                  title: "Name",
                  value: model.organization?.name ?? ""
                ),
                CustomRow(
                  title: "Country",
                  value: model.organization?.country ?? ""
                ),
                CustomRow(
                  title: "City",
                  value: model.organization?.city ?? ""
                ),
                CustomRow(
                  title: "State",
                  value: model.organization?.state ?? ""
                ),
              ],
            ),
            contentBackgroundColor: contentBg,
          ),
        );

        final issuerDetails = Container(
          child: ExpandedTile(
            controller: ExpandedTileController(),
            leading: Icon(FlutterIcons.user_check_fea, color: iconColor),
            centerHeaderTitle: false,
            title: Text("Issuer Details", style: headerTextStyle),
            content: Column(
              children: <Widget>[
                CustomRow(
                  title: "Name",
                  value: model.issuer?.fullname() ?? ""
                ),
                CustomRow(
                  title: "Medical ID",
                  value: model.issuer?.medicalId ?? ""
                ),
              ],
            ),
            contentBackgroundColor: contentBg,
          ),
        );

        return BaseView(
          isLoading: model.isLoading,
          title: "Prescription Details",
          body: SingleChildScrollView(
            child: Column(
              children: [
                basicDetails,
                medicationDetails,
                organizationDetails,
                issuerDetails,
              ],
            ),
          ),
        );
      },
    );
  }

  List<Widget> _buildMedications() {
    List<Widget> widgets = [];
    var medications = prescription.medications;

    for (var med in medications) {
      // var index = medications.indexOf(med);
      widgets.add(
        MedicationTile(
          medication: med,
        ),
      );
    }

    return widgets;
  }
}
