import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import 'package:flutter_expanded_tile/tileController.dart';
import 'package:truya_pharmis/models/medication.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/views/components/custom_row.dart';

class MedicationTile extends StatelessWidget {
  final Medication medication;

  const MedicationTile({Key key, this.medication}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ExpandedTile(
        controller: ExpandedTileController(),
        centerHeaderTitle: false,
        title: Text(
          medication.itemDescription,
          style: TextStyle(
            color: Colors.black87,
            fontWeight: FontWeight.w600,
          ),
        ),
        content: Container(
          child: Column(
            children: [
              CustomRow(
                title: "Dosage",
                value: medication.dosage,
              ),
              CustomRow(
                title: "Frequency",
                value: medication.frequency,
              ),
              CustomRow(
                title: "Description",
                value: medication.itemDescription,
              ),
              CustomRow(
                title: "Start Date",
                value: Utils.formatDate(medication.startDate),
              ),
              CustomRow(
                title: "End Date",
                value: Utils.formatDate(medication.endDate),
              ),
            ],
          ),
        ),
        contentBackgroundColor: Colors.grey.withOpacity(0.05),
      ),
    );
  }
}
