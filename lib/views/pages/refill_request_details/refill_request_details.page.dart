import 'package:flutter/material.dart';
import 'package:flutter_expanded_tile/flutter_expanded_tile.dart';
import 'package:flutter_expanded_tile/tileController.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/models/refill_request.dart';
import 'package:truya_pharmis/util/utils.dart';
import 'package:truya_pharmis/view_models/refill_request_details.vm.dart';
import 'package:truya_pharmis/view_models/refill_requests.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/custom_btn.dart';
import 'package:truya_pharmis/views/components/custom_row.dart';

class RefillRequestDetailsPage extends StatelessWidget {
  final RefillRequest request;
  final int index;
  final RefillRequestsViewModel vm;

  const RefillRequestDetailsPage({
    Key key,
    this.request,
    this.index,
    this.vm,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<RefillRequestDetailsViewModel>.reactive(
      viewModelBuilder: () => RefillRequestDetailsViewModel(),
      onModelReady: (model) => model.initialize(context, request),
      builder: (context, model, child) {
        final contentBg = Colors.grey.withOpacity(0.05);
        final iconColor = Colors.black87;
        final headerTextStyle = TextStyle(
          color: Colors.black87,
          fontWeight: FontWeight.w600,
        );

        final basicDetails = Container(
          child: ExpandedTile(
            controller: ExpandedTileController(),
            leading: Icon(Icons.info_outline, color: iconColor),
            centerHeaderTitle: false,
            title: Text("Basic Info", style: headerTextStyle),
            content: Column(
              children: <Widget>[
                CustomRow(
                  title: "Comments",
                  value: request.comments,
                ),
                CustomRow(
                  title: "Request Date",
                  value: Utils.formatDateTime(request.requestDate),
                ),
                CustomRow(
                  title: "Status",
                  value: request.status,
                ),
              ],
            ),
            contentBackgroundColor: contentBg,
          ),
        );

        final userDetails = Container(
          child: ExpandedTile(
            controller: ExpandedTileController(),
            leading: Icon(FlutterIcons.user_fea, color: iconColor),
            centerHeaderTitle: false,
            title: Text("User Details", style: headerTextStyle),
            content: Column(
              children: <Widget>[
                CustomRow(title: "Name", value: model.user?.fullname() ?? ""),
              ],
            ),
            contentBackgroundColor: contentBg,
          ),
        );

        final issuerDetails = Container(
          child: ExpandedTile(
            controller: ExpandedTileController(),
            leading: Icon(FlutterIcons.user_check_fea, color: iconColor),
            centerHeaderTitle: false,
            title: Text("Issuer Details", style: headerTextStyle),
            content: Column(
              children: <Widget>[
                CustomRow(
                  title: "Name",
                  value: model.issuer?.fullname() ?? "",
                ),
                CustomRow(
                  title: "Medical ID",
                  value: model.issuer?.medicalId ?? "",
                ),
              ],
            ),
            contentBackgroundColor: contentBg,
          ),
        );

        final actions = Container(
          child: ExpandedTile(
            controller: ExpandedTileController(),
            leading: Icon(FlutterIcons.menu_open_mco, color: iconColor),
            centerHeaderTitle: false,
            title: Text("Actions", style: headerTextStyle),
            content: Column(
              children: <Widget>[
                CustomBtn(
                  text: "Approve",
                  isOutline: true,
                  color: Colors.green,
                  textColor: Colors.green,
                  onPressed: () async {
                    await vm.changeStatus(
                      request.id,
                      "APPROVED",
                      index,
                    );
                    model.updateStatus("APPROVED");
                  },
                ),
                CustomBtn(
                  text: "Reject",
                  color: Colors.red,
                  onPressed: () async {
                    await vm.changeStatus(
                      request.id,
                      "CANCELLED",
                      index,
                    );
                    model.updateStatus("CANCELLED");
                  },
                ),
              ],
            ),
            contentBackgroundColor: contentBg,
          ),
        );

        return BaseView(
          isLoading: model.isLoading,
          title: "Refill Request Details",
          body: SingleChildScrollView(
            child: Column(
              children: [
                basicDetails,
                userDetails,
                issuerDetails,
                Visibility(
                  visible: model.loggedInUser?.isMedic ?? false,
                  child: Visibility(
                    visible: request.status == "PENDING" && request.issuerId == model.loggedInUser.id,
                    child: actions,
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
