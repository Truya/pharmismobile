import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/services/bottom_sheet.service.dart';
import 'package:truya_pharmis/views/components/bs_option_tile.dart';

class PrescriptionOptionBottomSheet extends StatelessWidget {
  final ValueChanged<bool> createNew;
  final ValueChanged<Prescription> fromExisting;
  final AppUser user;

  const PrescriptionOptionBottomSheet({
    Key key,
    this.createNew,
    this.user,
    this.fromExisting,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          BsOptionTile(
            icon: FlutterIcons.plus_fea,
            title: "Create New",
            onTap: () {
              Navigator.pop(context);
            },
          ),
          BsOptionTile(
            icon: FlutterIcons.folder_plus_fea,
            title: "Create From Existing",
            onTap: () async {
              Navigator.pop(context);
              Prescription prescription = await BottomSheetService().showFindUserPrescriptions(
                title: "Enter user access code to continue",
                context: context,
                mainAxisSize: MainAxisSize.min,
                user: user,
              );
              if(prescription != null){
                fromExisting(prescription);
              }
            },
          ),
        ],
      ),
    );
  }
}
