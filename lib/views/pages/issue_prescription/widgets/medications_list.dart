import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/medication.dart';
import 'package:truya_pharmis/views/pages/issue_prescription/widgets/medication_tile.dart';

class MedicationsList extends StatelessWidget {
  final List<Medication> meds;
  final ValueChanged<int> onDeleteTap;
  final VoidCallback onAddTap;

  const MedicationsList({
    Key key,
    this.meds,
    this.onDeleteTap,
    this.onAddTap
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      child: ListView(
        children: _buildView(context),
      ),
    );
  }

  List<Widget> _buildView(context) {
    final title = Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Medications",
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            MaterialButton(
              padding: EdgeInsets.zero,
              color: Theme.of(context).accentColor,
              onPressed: onAddTap,
              shape: CircleBorder(),
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            )
          ],
        ),
        Divider(),
      ],
    );
    List<Widget> widgets = [];
    widgets.add(title);
    for (var med in meds) {
      var index = meds.indexOf(med);
      widgets.add(
        MedicationTile(
          medication: med,
          onDeleteClick: () => onDeleteTap(index),
        ),
      );
    }

    return widgets;
  }
}
