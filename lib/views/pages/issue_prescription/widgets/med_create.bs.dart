import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:intl/intl.dart';
import 'package:truya_pharmis/models/medication.dart';
import 'package:truya_pharmis/views/components/custom_btn.dart';
import 'package:truya_pharmis/views/components/custom_form_field.dart';

class MedCreateBottomSheet extends StatefulWidget {
  final ValueChanged<Medication> onAdd;

  const MedCreateBottomSheet({
    Key key,
    this.onAdd,
  }) : super(key: key);

  @override
  _MedCreateBottomSheetState createState() => _MedCreateBottomSheetState();
}

class _MedCreateBottomSheetState extends State<MedCreateBottomSheet> {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  TextEditingController descriptionCtrl = new TextEditingController();
  TextEditingController dosageCtrl = new TextEditingController();
  TextEditingController frequencyCtrl = new TextEditingController();
  TextEditingController startDateCtrl = new TextEditingController();
  TextEditingController endDateCtrl = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Container(
        child: Column(
          children: [
            CustomFormField(
              icon: FlutterIcons.info_fea,
              labelText: "Description",
              controller: descriptionCtrl,
              validator: (String value) {
                if (value.isEmpty || value == null)
                  return "Description is required.";

                return null;
              },
            ),
            CustomFormField(
              icon: FlutterIcons.medicinebox_ant,
              labelText: "Dosage",
              controller: dosageCtrl,
              validator: (String value) {
                if (value.isEmpty || value == null)
                  return "Dosage is required.";

                return null;
              },
            ),
            CustomFormField(
              icon: FlutterIcons.counter_mco,
              labelText: "Frequency",
              controller: frequencyCtrl,
              validator: (String value) {
                if (value.isEmpty || value == null)
                  return "Frequency is required.";

                return null;
              },
            ),
            Theme(
              data: ThemeData(
                primarySwatch: Colors.blue,
              ),
              child: Builder(
                builder: (BuildContext context) {
                  return CustomFormField(
                    icon: FlutterIcons.clock_fea,
                    labelText: "Start Date",
                    controller: startDateCtrl,
                    validator: (String value) {
                      if (value.isEmpty || value == null)
                        return "Start date is required.";

                      return null;
                    },
                    readOnly: true,
                    onTap: () async {
                      DateTime dateTime = await showDatePicker(
                        context: context,
                        initialDate: startDateCtrl.text.length != 0
                            ? DateTime.parse(startDateCtrl.text)
                            : DateTime.now(),
                        firstDate: DateTime.now(),
                        lastDate: DateTime.now().add(
                          Duration(days: 365),
                        ),
                      );

                      if (dateTime != null) {
                        startDateCtrl.text = formatDateTime(dateTime);
                      }
                    },
                  );
                },
              ),
            ),
            Theme(
              data: ThemeData(
                primarySwatch: Colors.blue,
              ),
              child: Builder(
                builder: (BuildContext context) {
                  return CustomFormField(
                    icon: FlutterIcons.clock_fou,
                    labelText: "End Date",
                    controller: endDateCtrl,
                    validator: (String value) {
                      if (value.isEmpty || value == null)
                        return "End date is required.";

                      return null;
                    },
                    readOnly: true,
                    onTap: () async {
                      DateTime dateTime = await showDatePicker(
                        context: context,
                        initialDate: endDateCtrl.text.length != 0
                            ? DateTime.parse(endDateCtrl.text)
                            : DateTime.now(),
                        firstDate: DateTime.now(),
                        lastDate: DateTime.now().add(
                          Duration(days: 365),
                        ),
                      );

                      if (dateTime != null) {
                        endDateCtrl.text = formatDateTime(dateTime);
                      }
                    },
                  );
                },
              ),
            ),
            CustomBtn(
              text: "Add",
              onPressed: () {
                if (formKey.currentState.validate()) {
                  Medication med = Medication(
                    itemDescription: descriptionCtrl.text,
                    dosage: dosageCtrl.text,
                    frequency: frequencyCtrl.text,
                    startDate: DateTime.parse(startDateCtrl.text),
                    endDate: DateTime.parse(endDateCtrl.text),
                  );
                  widget.onAdd(med);
                  descriptionCtrl.clear();
                  dosageCtrl.clear();
                  frequencyCtrl.clear();
                  Navigator.pop(context);
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  String formatDateTime(DateTime date){
    return DateFormat("yyyy-MM-dd").format(date);
  }
}
