import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/view_models/issue_prescription.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/components/custom_btn.dart';
import 'package:truya_pharmis/views/components/custom_form_field.dart';
import 'package:truya_pharmis/views/pages/issue_prescription/widgets/medications_list.dart';

class IssuePrescriptionPage extends StatelessWidget {
  final AppUser user;
  final Prescription prescription;

  const IssuePrescriptionPage({
    Key key,
    this.user,
    this.prescription
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<IssuePrescriptionViewModel>.reactive(
      viewModelBuilder: () => IssuePrescriptionViewModel(),
      onModelReady: (model) => model.initialize(context, user, prescription),
      builder: (context, model, child) {
        final descriptionField = CustomFormField(
          icon: FlutterIcons.info_fea,
          labelText: "Description",
          controller: model.descriptionCtrl,
          enabled: !model.isLoading,
          maxLines: 1,
          validator: (String value) {
            if (value.isEmpty || value == null)
              return "Description is required.";

            return null;
          },
        );

        final notesField = CustomFormField(
          icon: FlutterIcons.notes_medical_faw5s,
          labelText: "Notes",
          controller: model.notesCtrl,
          enabled: !model.isLoading,
          maxLines: 2,
          validator: (String value) {
            

            return null;
          },
        );

        final button = CustomBtn(
          text: "Prescribe",
          isLoading: model.isLoading,
          onPressed: () => model.save(),
        );

        final medsList = Container(
          child: MedicationsList(
            meds: model.medications,
            onDeleteTap: model.onMedDeleteTap,
            onAddTap: model.onMedAddTap,
          ),
        );

        return BaseView(
          givePadding: true,
          isLoading: model.isLoading,
          title: "Issue Prescription",
          actions: [
            IconButton(
              icon: Icon(FlutterIcons.settings_fea),
              onPressed: () => model.showCreationOptions(),
            )
          ],
          body: Container(
            child: Form(
              key: model.formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  descriptionField,
                  notesField,
                  Expanded(
                    child: medsList,
                  ),
                  button,
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
