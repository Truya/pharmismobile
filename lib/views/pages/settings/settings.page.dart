import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:stacked/stacked.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/view_models/settings.vm.dart';
import 'package:truya_pharmis/views/components/base_view.dart';
import 'package:truya_pharmis/views/pages/appointment_settings/appointment_settings.page.dart';
import 'package:truya_pharmis/views/pages/home/tabs/account_tab/widgets/option_tile.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SettingsViewModel>.reactive(
      viewModelBuilder: () => SettingsViewModel(),
      onModelReady: (model) => model.initialise(context),
      builder: (context, model, child) {
        return BaseView(
          title: "Settings",
          body: Container(
            child: Column(
              children: [
                Visibility(
                  visible: model.loggedInUser?.isMedic ?? false,
                  child: OptionTile(
                    icon: FlutterIcons.calendar_oct,
                    iconColor: Colors.cyan,
                    title: "Appointment Settings",
                    onTap: () {
                      Navigate.pushPage(
                        context,
                        AppointmentSettingsPage(),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
