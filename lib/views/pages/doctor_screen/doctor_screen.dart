import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:truya_pharmis/views/pages/doctor_screen/doctor_details.dart';

class DoctorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Doctor'),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return DoctorDetails();
                  },
                ),
              );
            },
            child: Container(
              height: 80,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              padding: EdgeInsets.only(top: 10, left: 15, bottom: 10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(13),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 2,
                    color: Colors.grey[300],
                    spreadRadius: 1,
                  )
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 30,
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Stack(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Emmanuel Fache',
                                  style: TextStyle(
                                    color: Color(0xFF4B4949),
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Text(
                                  'Heart Surgeon - Flower Hospitals',
                                  maxLines: 1,
                                  style: TextStyle(
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            right: 1,
                            child: Row(
                              children: [
                                Icon(
                                  CupertinoIcons.chat_bubble,
                                  size: 20,
                                  color: Theme.of(context).accentColor,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(
                                  Icons.videocam_outlined,
                                  color: Theme.of(context).accentColor,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
