import 'package:flutter/material.dart';
import 'package:truya_pharmis/widgets/top_doctor_card.dart';

class DoctorDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dr. Emmanuel Fache'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Row(
                children: [
                  CircleAvatar(
                    radius: 40,
                  ),
                  SizedBox(width: 20),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.7,
                    child: Column(
                      children: [
                        Text(
                          'Heart Surgeon - Flower Hospitals',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            DoctorContactIcon(
                              icon: Icons.phone,
                            ),
                            SizedBox(width: 15),
                            DoctorContactIcon(
                              icon: Icons.message,
                            ),
                            SizedBox(width: 15),
                            DoctorContactIcon(
                              icon: Icons.videocam,
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Container(
                height: 30,
              ),
              Text(
                'About Doctor',
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF4B4949),
                  fontWeight: FontWeight.w700,
                ),
              ),
              Container(
                height: 10,
              ),
              Text(
                "Dr. Nony is the top most heart surgeon in Flower Hospital. He has done over 100 successful sugeries within past 3 years. He has achieved several awards for his wonderful contribution in her own field. He’s available for private consultation for given schedules.",
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF7C7C7C),
                  // fontWeight: FontWeight.w700,
                ),
              ),
              Container(
                height: 10,
              ),
              Text(
                'Upcoming Schedules',
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF4B4949),
                  fontWeight: FontWeight.w700,
                ),
              ),
              Container(
                height: 10,
              ),
              ListView.builder(
                shrinkWrap: true,
                itemCount: 5,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  return Container(
                    height: 80,
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(vertical: 10),
                    padding: EdgeInsets.only(top: 15, left: 15, bottom: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(13),
                      border: Border.all(
                        color: Color(0xFFB2AEAD),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              height: 55,
                              width: 55,
                              decoration: BoxDecoration(
                                color: Theme.of(context).accentColor,
                                borderRadius: BorderRadius.circular(7),
                              ),
                              child: Center(
                                child: Text(
                                  '01',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 24,
                                    fontWeight: FontWeight.w900,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 2.1,
                              // color: Colors.blue,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Nov 27, 2021  11: 37 AM',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  Text(
                                    '2 Medications',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w900,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Container(
                            height: 20,
                            width: 70,
                            decoration: BoxDecoration(
                              color: Theme.of(context).accentColor,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10),
                              ),
                            ),
                            child: Center(
                              child: Text(
                                'PENDING',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              // Flexible(
              //   child: ListView.builder(
              //     itemCount: 10,
              //     shrinkWrap: true,
              //     itemBuilder: (context, index) {
              //       return Container(
              //         height: 80,
              //         width: MediaQuery.of(context).size.width,
              //         margin: EdgeInsets.symmetric(vertical: 10),
              //         padding: EdgeInsets.only(top: 15, left: 15, bottom: 15),
              //         decoration: BoxDecoration(
              //           borderRadius: BorderRadius.circular(13),
              //           border: Border.all(
              //             color: Color(0xFFB2AEAD),
              //           ),
              //         ),
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //           children: [
              //             Row(
              //               children: [
              //                 Container(
              //                   height: 55,
              //                   width: 55,
              //                   decoration: BoxDecoration(
              //                     color: Theme.of(context).accentColor,
              //                     borderRadius: BorderRadius.circular(7),
              //                   ),
              //                   child: Center(
              //                     child: Text(
              //                       '01',
              //                       style: TextStyle(
              //                         color: Colors.white,
              //                         fontSize: 24,
              //                         fontWeight: FontWeight.w900,
              //                       ),
              //                     ),
              //                   ),
              //                 ),
              //                 SizedBox(
              //                   width: 20,
              //                 ),
              //                 Container(
              //                   width: MediaQuery.of(context).size.width / 2.1,
              //                   color: Colors.blue,
              //                   child: Column(
              //                     mainAxisAlignment:
              //                         MainAxisAlignment.spaceEvenly,
              //                     crossAxisAlignment: CrossAxisAlignment.start,
              //                     children: [
              //                       Text(
              //                         'Nov 27, 2021  11: 37 AM',
              //                         style: TextStyle(
              //                           color: Colors.black,
              //                           fontWeight: FontWeight.w600,
              //                         ),
              //                       ),
              //                       Text(
              //                         '2 Medications',
              //                         style: TextStyle(
              //                           fontWeight: FontWeight.w900,
              //                         ),
              //                       ),
              //                     ],
              //                   ),
              //                 ),
              //               ],
              //             ),
              //             Padding(
              //               padding: const EdgeInsets.only(top: 20.0),
              //               child: Container(
              //                 height: 20,
              //                 width: 70,
              //                 decoration: BoxDecoration(
              //                   color: Theme.of(context).accentColor,
              //                   borderRadius: BorderRadius.only(
              //                     topLeft: Radius.circular(10),
              //                     bottomLeft: Radius.circular(10),
              //                   ),
              //                 ),
              //                 child: Center(
              //                   child: Text(
              //                     'PENDING',
              //                     style: TextStyle(
              //                       color: Colors.white,
              //                       fontWeight: FontWeight.w600,
              //                     ),
              //                   ),
              //                 ),
              //               ),
              //             ),
              //           ],
              //         ),
              //       );
              //     },
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

class DoctorContactIcon extends StatelessWidget {
  final IconData icon;
  const DoctorContactIcon({
    Key key,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: 30,
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Center(
        child: Icon(
          icon,
          color: Colors.white,
          size: 18,
        ),
      ),
    );
  }
}
