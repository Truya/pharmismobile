import 'package:flutter/material.dart';

class BottomBarProvider with ChangeNotifier {
  int selectedItem = 0;

  void changeSelectedItem(int newValue) {
    selectedItem = newValue;
    notifyListeners();
  }
}
