import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:truya_pharmis/constants/constants.dart';
import 'package:truya_pharmis/constants/strings.dart';
import 'package:truya_pharmis/providers/BottomBarProvider.dart';
import 'package:truya_pharmis/theme/theme_config.dart';
import 'package:truya_pharmis/views/pages/wrapper.dart';
import 'package:connectycube_sdk/connectycube_sdk.dart' as ConnectyCube;

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    ConnectyCube.init(
      AppConstants.connectycubeAppId,
      AppConstants.connectycubeAuthKey,
      AppConstants.connectycubeAuthSecret,
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => BottomBarProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: appName,
        theme: ThemeConfig.lightTheme,
        // darkTheme: ThemeConfig.darkTheme,
        home: Wrapper(),
      ),
    );
  }
}
