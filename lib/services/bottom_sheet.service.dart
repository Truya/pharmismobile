import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/views/components/base.bs.dart';
import 'package:truya_pharmis/views/components/bottom_sheets/find_user.bs.dart';
import 'package:truya_pharmis/views/components/bottom_sheets/find_user_prescriptions.bs.dart';

class BottomSheetService {
  Future show({
    @required BuildContext context,
    @required Widget body,
    String title,
    MainAxisSize mainAxisSize = MainAxisSize.min,
    bool showControl = true,
    EdgeInsets padding,
  }) {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setBsState) {
            return BaseBottomSheet(
              title: title,
              body: body,
              mainAxisSize: mainAxisSize,
              showControl: showControl,
              padding: padding ?? EdgeInsets.all(20.0),
            );
          },
        );
      },
    );
  }

  Future<AppUser> showFindUser({
    @required BuildContext context,
    String title,
    MainAxisSize mainAxisSize = MainAxisSize.min,
    bool showControl = true,
  }) {
    return showModalBottomSheet<AppUser>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return BaseBottomSheet(
          title: title,
          body: FindUserBottomSheet(),
          mainAxisSize: mainAxisSize,
          showControl: showControl,
        );
      },
    );
  }

  Future<Prescription> showFindUserPrescriptions({
    @required BuildContext context,
    String title,
    MainAxisSize mainAxisSize = MainAxisSize.min,
    bool showControl = true,
    AppUser user,
  }) {
    return showModalBottomSheet<Prescription>(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return BaseBottomSheet(
          title: title,
          body: FindUserPrescriptionsBottomSheet(
            user: user,
          ),
          mainAxisSize: mainAxisSize,
          showControl: showControl,
        );
      },
    );
  }
}
