import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';

enum AlertType { success, error, warning }

class AlertService {
  void show({
    @required String message,
    @required AlertType type,
    @required BuildContext context,
  }) {

    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.TOP,
      timeInSecForIosWeb: 2,
      backgroundColor: _chooseColor(type),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  void showPlatformError({
    @required exception,
    @required BuildContext context,
  }) {
    String message = "An unexpected error occured!";
    if (exception is String) {
      message = exception;
    } else if (exception is NoSuchMethodError) {
      // Do something later
    } else {
      message = exception.message;
      print(exception);
    }
    show(
      message: message,
      type: AlertType.error,
      context: context,
    );
  }

  Color _chooseColor(AlertType type) {
    if (type == AlertType.success) {
      return Colors.green;
    } else if (type == AlertType.error) {
      return Colors.red;
    } else {
      return Colors.orange;
    }
  }

  String chooseTitle(AlertType type) {
    if (type == AlertType.success) {
      return "Success!!!";
    } else if (type == AlertType.error) {
      return "Error!!!";
    } else {
      return "Warning!!!";
    }
  }

  IconData chooseIcon(AlertType type) {
    if (type == AlertType.success) {
      return Icons.check_circle_outline;
    } else if (type == AlertType.error) {
      return Icons.error_outline;
    } else {
      return Icons.info_outline;
    }
  }
}
