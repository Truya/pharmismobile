import 'package:firebase_auth/firebase_auth.dart';
import 'package:truya_pharmis/constants/firebase.dart';

class AuthService {
  Future<User> login(String email, String password) async {
    UserCredential authResult = await firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    User user = authResult.user;
    if (!user.emailVerified) await user.sendEmailVerification();
    return user;
  }

  logOut() async {
    await firebaseAuth.signOut();
  }

  String handleFirebaseAuthError(String e) {
    if (e.contains("weak-password")) {
      return "Password is weak, please input a new password";
    } else if (e.contains("invalid-email")) {
      return "Invalid Email";
    } else if (e.contains('email-already-in-use')) {
      return "The email address is already in use by another account.";
    } else if (e.contains("network-request-failed")) {
      return "Network error occured!";
    } else if (e.contains('firebase_auth/user-not-found')) {
      return "Invalid credentials.";
    } else if (e.contains('wrong-password')) {
      return "Invalid credentials.";
    } else {
      return e;
    }
  }

  Future<void> sendPasswordReset(String email) async {
    return firebaseAuth.sendPasswordResetEmail(email: email);
  }

  Future logout() async {
    return await firebaseAuth.signOut();
  }
}
