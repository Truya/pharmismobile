import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/constants/firestore_collections.dart';
import 'base.repository.dart';

class UserRepository extends BaseRepository {
  CollectionReference get colRef => db.collection(
        FirestoreCollections.users,
      );

  Future<DocumentReference> create(Map data) {
    return colRef.add(data);
  }

  Future<QuerySnapshot> list({
    String clinicId,
    String pharmacyId,
    bool isMedic,
    List list,
  }) {
    Query query = colRef;
    if (clinicId != null) {
      query = query.where("assignedClinicId", isEqualTo: clinicId);
    }
    if (isMedic != null) {
      query = query.where("isMedic", isEqualTo: isMedic);
    }
    if (list != null) {
      if (list.length != 0) {
        query = query.startAfterDocument(list[list.length - 1]);
      }
      query = query.limit(perPage);
    }
    return query.get();
  }

  Future<DocumentSnapshot> view(String id) async {
    return await colRef.doc(id).get();
  }

  Future<QuerySnapshot> findByEmail(String email) async {
    return await colRef.where("email", isEqualTo: email).get();
  }

  Future<QuerySnapshot> checkAccessCode(
      String userId, String accessCode) async {
    return await colRef
        .where("uid", isEqualTo: userId)
        .where("accessCode", isEqualTo: accessCode)
        .get();
  }

  Stream<DocumentSnapshot> viewStream(String id) {
    return colRef.doc(id).snapshots();
  }

  Future<void> update(String id, Map data) {
    return colRef.doc(id).update(data);
  }

  Future<void> delete(String id) {
    return colRef.doc(id).delete();
  }
}
