import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/constants/firestore_collections.dart';
import 'base.repository.dart';

class PrescriptionRepository extends BaseRepository {
  CollectionReference get colRef => db.collection(
        FirestoreCollections.prescriptions,
      );

  Future<DocumentReference> create(Map data) {
    return colRef.add(data);
  }

  Future<QuerySnapshot> list({
    String clinicId,
    String pharmacyId,
    String recipientId,
    String issuerId,
    List list,
  }) {
    Query query = colRef;
    if (clinicId != null) {
      query = query.where("clinicId", isEqualTo: clinicId);
    }
    if (recipientId != null) {
      query = query.where("recipientId", isEqualTo: recipientId);
    }
    if (issuerId != null) {
      query = query.where("issuerId", isEqualTo: issuerId);
    }
    if (list != null) {
      if (list.length != 0) {
        query = query.startAfterDocument(list[list.length - 1]);
      }
      query = query.limit(perPage);
    }
    query = query.orderBy("issueDate", descending: true);
    return query.get();
  }

  Future<DocumentSnapshot> view(String id) async {
    return await colRef.doc(id).get();
  }

  Future<void> update(String id, Map data) {
    return colRef.doc(id).update(data);
  }

  Future<void> delete(String id) {
    return colRef.doc(id).delete();
  }
}
