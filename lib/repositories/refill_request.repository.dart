import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/constants/firestore_collections.dart';
import 'base.repository.dart';

class RefillRequestRepository extends BaseRepository {
  CollectionReference get colRef => db.collection(
        FirestoreCollections.refillRequests,
      );

  Future<DocumentReference> create(Map data) {
    return colRef.add(data);
  }

  Future<QuerySnapshot> list({
    String clinicId,
    String prescriptionId,
    String userId,
    String issuerId,
    String status,
    List list,
  }) {
    Query query = colRef;
    if (clinicId != null) {
      query = query.where("clinicId", isEqualTo: clinicId);
    }
    if (prescriptionId != null) {
      query = query.where("prescriptionId", isEqualTo: prescriptionId);
    }
    if (userId != null) {
      query = query.where("userId", isEqualTo: userId);
    }
    if (issuerId != null) {
      query = query.where("issuerId", isEqualTo: issuerId);
    }
    if (status != null) {
      if (status != "ALL") {
        query = query.where("status", isEqualTo: status);
      }
    }
    if (list != null) {
      if (list.length != 0) {
        query = query.startAfterDocument(list[list.length - 1]);
      }
      query = query.limit(perPage);
    }
    query = query.orderBy("requestDate", descending: true);
    return query.get();
  }

  Future<DocumentSnapshot> view(String id) async {
    return await colRef.doc(id).get();
  }

  Future<void> update(String id, Map data) {
    return colRef.doc(id).update(data);
  }

  Future<void> delete(String id) {
    return colRef.doc(id).delete();
  }

  Future<void> changeStatus(String id, String status) {
    return colRef.doc(id).update({"status": status});
  }
}
