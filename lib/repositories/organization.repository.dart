import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/constants/firestore_collections.dart';
import 'base.repository.dart';

class OgranizationRepository extends BaseRepository {
  CollectionReference get colRef => db.collection(
        FirestoreCollections.organizations,
      );

  Future<DocumentReference> create(Map data) {
    return colRef.add(data);
  }

  Future<QuerySnapshot> list({List list}) {
    Query query = colRef;
    if (list != null) {
      if (list.length != 0) {
        query = query.startAfterDocument(list[list.length - 1]);
      }
      query = query.limit(perPage);
    }
    return query.get();
  }

  Future<DocumentSnapshot> view(String id) async {
    return await colRef.doc(id).get();
  }
}
