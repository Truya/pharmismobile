import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/constants/firestore_collections.dart';
import 'base.repository.dart';

class CallLogRepository extends BaseRepository {
  CollectionReference get colRef => db.collection(
        FirestoreCollections.callLogs,
      );

  Future<DocumentReference> create(Map data) {
    return colRef.add(data);
  }

  Future<QuerySnapshot> list({
    String doctorId,
    String patientId,
    List list,
  }) {
    Query query = colRef;
    if (doctorId != null) {
      query = query.where("doctorId", isEqualTo: doctorId);
    }
    if (patientId != null) {
      query = query.where("patientId", isEqualTo: patientId);
    }
    if (list != null) {
      if (list.length != 0) {
        query = query.startAfterDocument(list[list.length - 1]);
      }
      query = query.limit(perPage);
    }
    query = query.orderBy("startTime", descending: true);
    return query.get();
  }

  Future<DocumentSnapshot> view(String id) async {
    return await colRef.doc(id).get();
  }

  Future<QuerySnapshot> findByEmail(String email) async {
    return await colRef.where("email", isEqualTo: email).get();
  }

  Stream<DocumentSnapshot> viewStream(String id) {
    return colRef.doc(id).snapshots();
  }

  Future<void> update(String id, Map data) {
    return colRef.doc(id).update(data);
  }

  Future<void> delete(String id) {
    return colRef.doc(id).delete();
  }
}
