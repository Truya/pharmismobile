import 'package:flutter/material.dart';
import 'package:truya_pharmis/constants/images.dart';

class ElevatedCircleAvatar extends StatelessWidget {
  final String image;
  const ElevatedCircleAvatar({
    Key key,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
            blurRadius: 20,
            color: Colors.grey[200],
            spreadRadius: 5,
          )
        ],
      ),
      child: CircleAvatar(
        radius: 30,
        backgroundColor: Colors.white,
        child: Image.asset(image),
      ),
    );
  }
}
