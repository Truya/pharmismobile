import 'package:flutter/material.dart';

class ProfileTile extends StatelessWidget {
  final Icon icon;
  final String title;
  const ProfileTile({
    Key key,
    this.icon,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: CircleAvatar(
            backgroundColor: Theme.of(context).accentColor,
            child: icon,
          ),
          title: Text(
            title,
            style: TextStyle(
              fontSize: 18,
            ),
          ),
          trailing: Icon(Icons.arrow_forward_ios),
        ),
        Divider(
          thickness: 1,
          indent: 10,
          endIndent: 10,
        ),
      ],
    );
  }
}
