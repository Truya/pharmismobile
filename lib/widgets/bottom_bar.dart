import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:truya_pharmis/constants/images.dart';

class BottomBar extends StatelessWidget {
  final int currentIndex;
  final Function onTap;
  const BottomBar({
    Key key,
    this.currentIndex,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: currentIndex,
      onTap: onTap,
      backgroundColor: Theme.of(context).accentColor,
      selectedItemColor: Color(0xFF1A1C4E),
      items: [
        BottomNavigationBarItem(
          label: 'Home',
          activeIcon: Image.asset(
            AppImages.homeIcon,
            color: Colors.black,
          ),
          icon: Image.asset(AppImages.homeIcon),
        ),
        BottomNavigationBarItem(
          label: 'Doctor',
          activeIcon: Image.asset(
            AppImages.doctorIcon,
            color: Colors.black,
          ),
          icon: Image.asset(AppImages.doctorIcon),
        ),
        BottomNavigationBarItem(
          label: 'Prescriptions',
          activeIcon: Image.asset(
            AppImages.prescriptionsIcon,
            color: Colors.black,
          ),
          icon: Image.asset(AppImages.prescriptionsIcon),
        ),
        BottomNavigationBarItem(
          label: 'Refil Req...',
          activeIcon: Image.asset(
            AppImages.refillIcon,
            color: Colors.black,
          ),
          icon: Image.asset(AppImages.refillIcon),
        ),
        BottomNavigationBarItem(
          label: 'Profile ',
          activeIcon: Image.asset(
            AppImages.profileIcon,
            color: Colors.black,
          ),
          icon: Image.asset(AppImages.profileIcon),
        ),
      ],
    );
  }
}
