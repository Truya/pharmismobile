import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final String title;
  final double width;
  final double height;
  final String hintText;
  final TextEditingController controller;
  final int minLines;
  final int maxLines;
  final bool obscureText;
  final bool enabled;
  final FormFieldValidator<String> validateFunction;
  final void Function(String) onSaved, onChange;
  final TextInputType textInputType;
  final TextInputAction textInputAction;
  final FocusNode focusNode, nextFocusNode;
  final VoidCallback submitAction;
  final bool enableErrorMessage;
  final bool streamText;

  const CustomTextField({
    Key key,
    this.title = 'Type Here',
    this.width,
    this.height,
    this.hintText,
    this.controller,
    this.minLines,
    this.maxLines = 1,
    this.obscureText = false,
    this.enabled,
    this.validateFunction,
    this.onSaved,
    this.onChange,
    this.textInputType,
    this.textInputAction,
    this.focusNode,
    this.nextFocusNode,
    this.submitAction,
    this.enableErrorMessage,
    this.streamText,
  }) : super(key: key);
  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  String error;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: TextStyle(
            color: Color(0x99011947),
            fontSize: 11,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(
          height: 5.0,
        ),
        Container(
          height: 60,
          padding: EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
            color: Color(0x08013088),
            border: Border.all(
              color: Color(0xFF013088).withOpacity(0.4),
            ),
            borderRadius: BorderRadius.circular(12),
          ),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: TextFormField(
                    textCapitalization: TextCapitalization.sentences,
                    // initialValue: widget.initialValue,
                    enabled: widget.enabled,
                    onChanged: (val) {
                      error = widget.validateFunction(val);
                      setState(() {});
                      if (widget.streamText == true) {
                        widget.onChange(val);
                      }
                      // widget.onSaved(val);
                    },
                    style: TextStyle(
                      fontSize: 15.0,
                    ),
                    key: widget.key,
                    maxLines: widget.maxLines,
                    controller: widget.controller,
                    obscureText: widget.obscureText,
                    keyboardType: widget.textInputType,
                    validator: widget.validateFunction,
                    onSaved: (val) {
                      error = widget.validateFunction(val);
                      setState(() {});
                      widget.onSaved(val);
                    },
                    textInputAction: widget.textInputAction,
                    focusNode: widget.focusNode,
                    onFieldSubmitted: (String term) {
                      if (widget.nextFocusNode != null) {
                        widget.focusNode.unfocus();
                        FocusScope.of(context)
                            .requestFocus(widget.nextFocusNode);
                      } else {
                        widget.submitAction();
                      }
                    },
                    decoration: InputDecoration(
                      filled: false,
                      hintText: widget.hintText,
                      hintStyle: TextStyle(
                        color: Colors.grey[400],
                      ),
                      disabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      errorBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      errorStyle: TextStyle(height: 0.0, fontSize: 0.0),
                    ),
                  ),
                ),
                widget.obscureText
                    ? Icon(
                        Icons.remove_red_eye_sharp,
                        color: Colors.grey,
                      )
                    : Container(),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
