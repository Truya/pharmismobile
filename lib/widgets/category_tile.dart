import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class CategoryTile extends StatelessWidget {
  final String image;
  final String label;
  const CategoryTile({
    Key key,
    this.image,
    this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: 110,
      // color: Colors.blue,
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            child: Container(
              height: 135,
              width: 100,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 20,
                    color: Colors.grey[200],
                    spreadRadius: 5,
                  )
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, bottom: 10),
                    child: Text(
                      label,
                      style: TextStyle(
                        fontSize: 16,
                        height: 1,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 0,
            child: Container(
              height: 80,
              width: 78,
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Center(
                child: Image.asset(image),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
