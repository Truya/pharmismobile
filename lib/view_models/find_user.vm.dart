import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'base.vm.dart';

class FindUserViewModel extends BaseViewModel {
  // FormKey
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  // TextEditingControllers
  TextEditingController emailCtrl = TextEditingController();

  // Repositories
  UserRepository userRepo = new UserRepository();

  // Variables
  AppUser user;
  List<AppUser> users;
  bool hasLoaded = false;
  bool notFound = false;

  @override
  void initialise(context) {
    users = [];
    hasLoaded = false;
    notFound = false;
    super.initialise(context);
  }

  @override
  void reset() {
    super.reset();
  }

  void find() async {
    if (this.formKey.currentState.validate()) {
      user = null;
      try {
        setLoading(true);
        QuerySnapshot data = await userRepo.findByEmail(emailCtrl.text);
        hasLoaded = true;
        if (data.docs.length != 0) {
          user = AppUser.fromDoc(data.docs.first);
          notFound = false;
        } else {
          notFound = true;
        }
        setLoading(false);
      } catch (e) {
        print(e);
        alert.showPlatformError(
          exception: e,
          context: context,
        );
      }
      setLoading(false);
    }
  }

  void onUserTap(AppUser user) {
    // Do something
    notifyListeners();
  }
}
