import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/repositories/prescription.repository.dart';
import 'base.vm.dart';

class PrescriptionsViewModel extends BaseViewModel {
  // FormKey
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  // Variables
  List<Prescription> yourPrescriptions;
  List<DocumentSnapshot> yourPrescriptionsDocs;

  List<Prescription> issuedPrescriptions;
  List<DocumentSnapshot> issuedPrescriptionsDocs;

  AppUser loggedInUser;

  // Repositories
  PrescriptionRepository prescRepo = new PrescriptionRepository();

  @override
  void initialise(context) async {
    yourPrescriptions = [];
    yourPrescriptionsDocs = [];

    issuedPrescriptions = [];
    issuedPrescriptionsDocs = [];

    loggedInUser = await getLoggedInUser();
    getYourPrescriptions();
    getIssuedPrescriptions();
    super.initialise(context);
  }

  @override
  void reset() {
    yourPrescriptions = [];
    yourPrescriptionsDocs = [];

    notifyListeners();
    super.reset();
  }

  void resetIssuedPrescriptions() {
    issuedPrescriptions = [];
    issuedPrescriptionsDocs = [];

    notifyListeners();
    super.reset();
  }

  @override
  void refresh() async {
    super.refresh();
    getYourPrescriptions();
  }

  void refreshIssuedPrescriptions() async {
    resetIssuedPrescriptions();
    getIssuedPrescriptions();
  }

  Future getYourPrescriptions() async {
    setListLoading(true);
    QuerySnapshot data = await prescRepo.list(
      recipientId: prescRepo.auth.currentUser.uid,
    );
    setListLoading(false);
    yourPrescriptionsDocs.addAll(data.docs);
    yourPrescriptions.addAll(
      yourPrescriptionsDocs.map((e) => Prescription.fromDoc(e)).toList(),
    );
    notifyListeners();
  }

  Future getIssuedPrescriptions() async {
    setListLoading(true);
    QuerySnapshot data = await prescRepo.list(
      issuerId: prescRepo.auth.currentUser.uid,
    );
    setListLoading(false);
    issuedPrescriptionsDocs.addAll(data.docs);
    issuedPrescriptions.addAll(
      issuedPrescriptionsDocs.map((e) => Prescription.fromDoc(e)).toList(),
    );
    notifyListeners();
  }
}
