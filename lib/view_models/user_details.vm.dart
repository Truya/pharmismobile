import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'base.vm.dart';

class UserDetailsViewModel extends BaseViewModel {
  // Variables
  AppUser user;
  AppUser loggedInUser;

  // Repositories
  UserRepository userRepo = new UserRepository();

  void initialize(context, AppUser _user) async {
    initialise(context);
    user = _user;

    // getIssuerAndOrg();

    loggedInUser = await getLoggedInUser();
  }

  // getIssuerAndOrg() async {
  //   var doc = await userRepo.view(prescription.issuerId);
  //   issuer = AppUser.fromDoc(doc);

  //   var doc2 = await orgRepo.view(issuer.orgId);
  //   if (doc2.exists) {
  //     organization = Organization.fromDoc(doc2);
  //   }

  //   notifyListeners();
  // }
}
