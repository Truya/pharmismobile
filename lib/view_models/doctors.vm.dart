import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'base.vm.dart';

class DoctorsViewModel extends BaseViewModel {
  // Variables
  List<AppUser> doctors;
  List<DocumentSnapshot> doctorsDoc;

  AppUser loggedInUser;

  UserRepository userRepo = new UserRepository();

  @override
  void initialise(context) async {
    doctors = [];
    doctorsDoc = [];

    loggedInUser = await getLoggedInUser();
    index();
    super.initialise(context);
  }

  @override
  void reset() {
    doctors = [];
    doctorsDoc = [];

    notifyListeners();
    super.reset();
  }

  void refresh() async {
    super.refresh();
    index();
  }

  index() async {
    setListLoading(true);
    QuerySnapshot data = await userRepo.list(
      isMedic: true,
    );
    setListLoading(false);
    doctorsDoc.addAll(data.docs);
    doctors.addAll(
      doctorsDoc.map((e) => AppUser.fromDoc(e)).toList(),
    );
    notifyListeners();
  }
}
