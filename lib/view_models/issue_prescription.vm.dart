import 'package:flutter/widgets.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/medication.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/repositories/prescription.repository.dart';
import 'package:truya_pharmis/services/alert.service.dart';
import 'package:truya_pharmis/views/pages/issue_prescription/widgets/med_create.bs.dart';
import 'package:truya_pharmis/views/pages/issue_prescription/widgets/prescription_option.bs.dart';
import 'base.vm.dart';

class IssuePrescriptionViewModel extends BaseViewModel {
  // FormKey
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  // TextEditingControllers
  TextEditingController descriptionCtrl = TextEditingController();
  TextEditingController notesCtrl = TextEditingController();

  // Repositories
  PrescriptionRepository prescRepo = new PrescriptionRepository();

  // Variables
  List<Medication> medications;
  AppUser user;
  Prescription selectedPrescription;

  @override
  void initialise(context) {
    medications = [];
    super.initialise(context);
  }

  void initialize(context, _user, _prescription) {
    initialise(context);
    user = _user;
    selectedPrescription = _prescription;
    if (_prescription == null) {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) => showCreationOptions(),
      );
    } else {
      WidgetsBinding.instance.addPostFrameCallback(
        (_) => autoPopulateForm(),
      );
    }
  }

  @override
  void reset() {
    descriptionCtrl.clear();
    notesCtrl.clear();
    medications.clear();
    super.reset();
  }

  void save() {
    if (this.formKey.currentState.validate() && medications.length != 0) {
      try {
        var meds = [];
        medications.forEach((e) {
          meds.add(e.toJson());
        });
        Prescription prescription = new Prescription(
          issuerId: prescRepo.auth.currentUser.uid,
          clinicId: "t3xlhO6l8jr6uJlEw80s",
          recipientId: user.id,
          description: descriptionCtrl.text,
          notes: notesCtrl.text,
          medications: meds,
          relatedTo:
              selectedPrescription == null ? null : selectedPrescription.id,
        );
        setLoading(true);
        prescRepo.create(prescription.toJson());
        reset();
        setLoading(false);
        Navigator.pop(context, true);

        alert.show(
          message: "Prescription has been issued!",
          type: AlertType.success,
          context: context,
        );
      } catch (e) {
        print(e);
        alert.showPlatformError(
          exception: e,
          context: context,
        );
      }
      setLoading(false);
    } else {
      if (medications.length == 0) {
        alert.show(
          message: "Enter at least one medication",
          type: AlertType.error,
          context: context,
        );
      }
    }
  }

  void onMedDeleteTap(int index) {
    medications.removeAt(index);
    notifyListeners();
  }

  void onMedAddTap() {
    bottomSheet.show(
      context: context,
      title: "Add Medication",
      mainAxisSize: MainAxisSize.min,
      body: MedCreateBottomSheet(
        onAdd: (Medication medication) {
          medications.add(medication);
          notifyListeners();
        },
      ),
    );
  }

  void showCreationOptions() {
    bottomSheet.show(
      context: context,
      showControl: false,
      body: PrescriptionOptionBottomSheet(
        user: user,
        fromExisting: (Prescription p) {
          // Autopopulate form
          selectedPrescription = p;
          autoPopulateForm();
        },
      ),
    );
  }

  void autoPopulateForm() {
    descriptionCtrl.text = selectedPrescription.description;
    notesCtrl.text = selectedPrescription.notes;
    medications = selectedPrescription.medications;
    notifyListeners();
  }
}
