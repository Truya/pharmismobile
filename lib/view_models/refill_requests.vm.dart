import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/models/refill_request.dart';
import 'package:truya_pharmis/repositories/prescription.repository.dart';
import 'package:truya_pharmis/repositories/refill_request.repository.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'package:truya_pharmis/services/alert.service.dart';
import 'package:truya_pharmis/util/navigate.dart';
import 'package:truya_pharmis/views/pages/issue_prescription/issue_prescription.page.dart';
import 'base.vm.dart';

class RefillRequestsViewModel extends BaseViewModel {
  // Variables
  List<RefillRequest> requests;
  List<DocumentSnapshot> requestsDoc;

  List<RefillRequest> patientsRequests;
  List<DocumentSnapshot> patientsRequestsDoc;

  List<String> statusList = [];
  String selectedStatus;

  AppUser loggedInUser;

  // Repositories
  RefillRequestRepository requestRepo = new RefillRequestRepository();
  UserRepository userRepo = new UserRepository();
  PrescriptionRepository prescRepo = new PrescriptionRepository();

  @override
  void initialise(context) async {
    requests = [];
    requestsDoc = [];

    patientsRequests = [];
    patientsRequestsDoc = [];

    statusList = ["ALL", "APPROVED", "PENDING", "CANCELLED"];
    selectedStatus = "ALL";

    loggedInUser = await getLoggedInUser();
    index();
    super.initialise(context);
  }

  @override
  void reset() {
    yourRequestsReset();
    patientsRequestsReset();
    super.reset();
  }

  void yourRequestsReset() {
    requests = [];
    requestsDoc = [];

    notifyListeners();
  }

  void patientsRequestsReset() {
    patientsRequests = [];
    patientsRequestsDoc = [];

    notifyListeners();
  }

  void yourRequestRefresh() async {
    yourRequestsReset();
    getYourRequests();
  }

  void patientsRequestRefresh() async {
    patientsRequestsReset();
    getPatientsRequests();
  }

  refreshOne(bool patients) {
    if (patients) {
      patientsRequestRefresh();
    } else {
      yourRequestRefresh();
    }
  }

  index() {
    getPatientsRequests();
    getYourRequests();
  }

  Future getYourRequests() async {
    setListLoading(true);
    QuerySnapshot data = await requestRepo.list(
      userId: requestRepo.auth.currentUser.uid,
      status: selectedStatus,
    );
    setListLoading(false);
    requestsDoc.addAll(data.docs);
    requests.addAll(
      requestsDoc.map((e) => RefillRequest.fromDoc(e)).toList(),
    );
    notifyListeners();
  }

  Future getPatientsRequests() async {
    setListLoading(true);
    QuerySnapshot data = await requestRepo.list(
      issuerId: requestRepo.auth.currentUser.uid,
      status: selectedStatus,
    );
    setListLoading(false);
    patientsRequestsDoc.addAll(data.docs);
    patientsRequests.addAll(
      patientsRequestsDoc.map((e) => RefillRequest.fromDoc(e)).toList(),
    );
    notifyListeners();
  }

  Future changeStatus(String id, String status, int i) async {
    try {
      if (status == "APPROVED") {
        var selectedRequest = patientsRequests[i];
        var user = AppUser.fromDoc(
          await userRepo.view(selectedRequest.userId),
        );
        var prescription = Prescription.fromDoc(
          await prescRepo.view(selectedRequest.prescriptionId),
        );
        print(prescription.id);
        bool didPrescribe = await Navigate.pushPage(
          context,
          IssuePrescriptionPage(
            user: user,
            prescription: prescription,
          ),
        );
        if (didPrescribe != null) {
          doStatusChange(id, status, i);
        } else {
          // They did not prescribe
          // print("DID not prescribe");
        }
      } else {
        doStatusChange(id, status, i);
      }
    } catch (e) {
      alert.showPlatformError(
        exception: e,
        context: context,
      );
    }
    setLoading(false);
  }

  void doStatusChange(String id, String status, int i) {
    setLoading(true);
    requestRepo.changeStatus(id, status);
    setLoading(false);

    alert.show(
      message: "Status changed",
      type: AlertType.success,
      context: context,
    );

    requests[i].status = status;
    notifyListeners();
  }

  void onStatusChanged(String status) {
    selectedStatus = status;
    notifyListeners();
  }
}
