import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:truya_pharmis/services/auth_service.dart';

class EditProfileViewModel extends ChangeNotifier {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  FocusNode emailFN = FocusNode();
  FocusNode passFN = FocusNode();
  String name, email, password;
  bool loading = false;
  AuthService auth = AuthService();

  submit(BuildContext context) async {
    FormState form = formKey.currentState;
    form.save();
    if (!form.validate()) {
      notifyListeners();
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      unFocusNodes();
      loading = true;
      notifyListeners();
      try {
        // Save profile
      } catch (e) {
        loading = false;
        notifyListeners();
        print(e);
        showInSnackBar('${auth.handleFirebaseAuthError(e.toString())}');
      }
      loading = false;
      notifyListeners();
    }
  }

  setEmail(var val) {
    email = val;
    notifyListeners();
  }

  setPassword(var val) {
    password = val;
    notifyListeners();
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState.removeCurrentSnackBar();
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(value)));
  }

  unFocusNodes() {
    emailFN.unfocus();
    passFN.unfocus();
  }
}
