import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'package:truya_pharmis/services/alert.service.dart';
import 'package:truya_pharmis/services/bottom_sheet.service.dart';

class BaseViewModel with ChangeNotifier {
  // Services
  final alert = new AlertService();
  final bottomSheet = new BottomSheetService();

  // Variables
  bool _isLoading = false;
  bool _listLoading = false;
  BuildContext _context;
  bool _firstTimeDataLoad = true;

  // Getters
  bool get isLoading => _isLoading;
  bool get listLoading => _listLoading;
  BuildContext get context => _context;

  // Functions
  void setLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  void setListLoading(bool value) {
    if (_firstTimeDataLoad) {
      _listLoading = value;
      _firstTimeDataLoad = false;
    } else {
      _listLoading = false;
    }
    _isLoading = value;
    notifyListeners();
  }

  void initialise(BuildContext context) {
    _context = context;
  }

  void reset() {}

  void refresh() {
    reset();
  }

  Future<AppUser> getLoggedInUser() async {
    var user = await UserRepository().view(
      FirebaseAuth.instance.currentUser.uid,
    );
    if (user != null) {
      return AppUser.fromDoc(user);
    }
    return null;
  }
}
