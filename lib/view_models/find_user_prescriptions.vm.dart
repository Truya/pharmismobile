import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/repositories/prescription.repository.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'package:truya_pharmis/services/alert.service.dart';
import 'base.vm.dart';

class FindUserPrescriptionsViewModel extends BaseViewModel {
  // FormKey
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  // TextEditingControllers
  TextEditingController codeCtrl = TextEditingController();

  // Repositories
  UserRepository userRepo = new UserRepository();
  PrescriptionRepository prescRepo = new PrescriptionRepository();

  // Variables
  Prescription selectedPrescription;
  List<Prescription> prescriptions;
  AppUser user;
  bool hasLoaded = false;

  @override
  void initialise(context) {
    prescriptions = [];
    hasLoaded = false;
    super.initialise(context);
  }

  void initialize(context, AppUser _user) {
    user = _user;
    initialise(context);
  }

  @override
  void reset() {
    super.reset();
  }

  void find() async {
    if (this.formKey.currentState.validate()) {
      prescriptions = [];
      try {
        setLoading(true);
        hasLoaded = true;
        // Confirm user code
        QuerySnapshot data = await userRepo.checkAccessCode(
          user.id,
          codeCtrl.text,
        );

        // If code is correct, display list to prescriptions
        if (data.docs.length != 0) {
          // Exists
          // Fetch prescriptions
          QuerySnapshot data2 = await prescRepo.list(
            recipientId: user.id,
          );
          prescriptions.addAll(
            data2.docs.map((e) => Prescription.fromDoc(e)).toList(),
          );
        } else {
          alert.show(
            message: "Sorry the code is invalid!",
            type: AlertType.error,
            context: context,
          );
        }
        setLoading(false);
      } catch (e) {
        print(e);
        alert.showPlatformError(
          exception: e,
          context: context,
        );
      }
      setLoading(false);
    }
  }

  void onPrescTap(Prescription prescription) {
    selectedPrescription = prescription;
    notifyListeners();
  }
}
