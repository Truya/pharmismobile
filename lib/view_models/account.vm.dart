import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/repositories/call_log.respository.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';

import 'base.vm.dart';

class AccountViewModel extends BaseViewModel {
  // Variables
  Stream<DocumentSnapshot> currentUserStream;
  AppUser loggedInUser;

  // Repositories
  UserRepository userRepo = new UserRepository();
  CallLogRepository callLogRepo = new CallLogRepository();

  @override
  initialise(context) async {
    currentUserStream = userRepo.viewStream(userRepo.auth.currentUser.uid);

    loggedInUser = await getLoggedInUser();
    notifyListeners();
  }

  @override
  void reset() {
    super.reset();
  }
}
