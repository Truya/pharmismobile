import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/call_log.dart';
import 'package:truya_pharmis/repositories/call_log.respository.dart';

import 'base.vm.dart';

class CallLogsViewModel extends BaseViewModel {
  AppUser loggedInUser;

  // Variables
  List<CallLog> callLogs = [];
  List<DocumentSnapshot> callLogsDoc = [];

  // Repositories
  CallLogRepository callLogRepo = new CallLogRepository();

  @override
  initialise(context) async {
    loggedInUser = await getLoggedInUser();
    index();
    super.initialise(context);
  }

  @override
  void reset() {
    callLogs = [];
    callLogsDoc = [];

    notifyListeners();
    super.reset();
  }

  void refresh() async {
    super.refresh();
    index();
  }

  index() async {
    setListLoading(true);
    QuerySnapshot data = await callLogRepo.list(
      doctorId: loggedInUser.isMedic ? loggedInUser.id : null,
      patientId: loggedInUser.isMedic ? null : loggedInUser.id,
    );
    setListLoading(false);
    callLogsDoc.addAll(data.docs);
    callLogs.addAll(
      callLogsDoc.map((e) => CallLog.fromDoc(e)).toList(),
    );
    notifyListeners();
  }
}
