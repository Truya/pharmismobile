import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:truya_pharmis/services/alert.service.dart';
import 'package:truya_pharmis/services/auth_service.dart';
import 'package:truya_pharmis/view_models/base.vm.dart';

class AuthViewModel extends BaseViewModel {
  // FormKey
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  // TextEditingControllers
  TextEditingController emailCtrl = TextEditingController();
  TextEditingController passwordCtrl = TextEditingController();

  // Services
  AuthService auth = new AuthService();

  @override
  void reset() {
    emailCtrl.clear();
    passwordCtrl.clear();
    super.reset();
  }

  login() async {
    if (this.formKey.currentState.validate()) {
      setLoading(true);
      try {
        User user = await auth.login(emailCtrl.text, passwordCtrl.text);
        if (user != null) {
          // AppUser appUser = AppUser.fromDoc(
          //   await UserRepository().view(user.uid),
          // );
          // If user has no connectycube id then create one for them
          // if (appUser.connectyCubeId == null) {
          //   createSession();
          //   // Create account
          //   CubeUser cUser = CubeUser(
          //     login: appUser.email,
          //     email: appUser.email,
          //     fullName: appUser.fullname(),
          //     password: appUser.id,
          //   );
          //   CubeUser nUser = await signUp(cUser);
          //   await UserRepository().update(user.uid, {"connectyCubeId": nUser.id});
          // }
          // // Login to Connectycube
          // await createSession(CubeUser(login: emailCtrl.text, password: user.uid));
          Navigator.pop(context);
        }
      } catch (e) {
        // alert.showPlatformError(
        //   exception: e,
        //   context: context,
        // );
        alert.show(
          message: auth.handleFirebaseAuthError(e.toString()),
          type: AlertType.error,
          context: context,
        );
      }
      setLoading(false);
    } else {
      alert.show(
        message: "Please fix the errors in red before submitting.",
        type: AlertType.error,
        context: context,
      );
    }
  }

  forgotPassword() async {
    if (this.formKey.currentState.validate()) {
      setLoading(true);
      auth.sendPasswordReset(emailCtrl.text);
      alert.show(
        message: 'Please check your email for a link to reset your '
            'password. Please also check your spam folder for the email',
        type: AlertType.success,
        context: context,
      );
      setLoading(false);
    } else {
      alert.show(
        message: "Please fix the errors in red before submitting.",
        type: AlertType.error,
        context: context,
      );
    }
  }
}
