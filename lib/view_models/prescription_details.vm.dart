import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/organization.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/repositories/organization.repository.dart';
import 'package:truya_pharmis/repositories/prescription.repository.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'base.vm.dart';

class PrescriptionDetailsViewModel extends BaseViewModel {
  // Variables
  Prescription prescription;
  AppUser issuer;
  Organization organization;

  AppUser loggedInUser;

  // Repositories
  PrescriptionRepository prescRepo = new PrescriptionRepository();
  UserRepository userRepo = new UserRepository();
  OgranizationRepository orgRepo = new OgranizationRepository();

  void initialize(context, Prescription _prescription) async {
    initialise(context);
    prescription = _prescription;

    getIssuerAndOrg();

    loggedInUser = await getLoggedInUser();
  }

  getIssuerAndOrg() async {
    var doc = await userRepo.view(prescription.issuerId);
    issuer = AppUser.fromDoc(doc);

    var doc2 = await orgRepo.view(issuer.orgId);
    if (doc2.exists) {
      organization = Organization.fromDoc(doc2);
    }

    notifyListeners();
  }
}
