import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/appointment.dart';
import 'package:truya_pharmis/models/availability.dart';
import 'package:truya_pharmis/models/available_at.dart';
import 'package:truya_pharmis/models/slot.dart';
import 'package:truya_pharmis/repositories/appointment.repository.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'package:truya_pharmis/services/alert.service.dart';
import 'package:truya_pharmis/util/utils.dart';

import 'base.vm.dart';

class BookAppointmentViewModel extends BaseViewModel {
  // Variables
  Stream<DocumentSnapshot> doctorStream;
  Stream<QuerySnapshot> appointmentsStream;
  AppUser selectedDoctor;
  AppUser loggedInUser;

  DateTime today = DateTime.now();
  Map<DateTime, List> events = {};
  List selectedEvents = [];
  List<Slot> userSelectedSlots = [];
  int maxDaysToEnable = 7;
  List<Appointment> appointments = [];

  // Repositories
  UserRepository userRepo = new UserRepository();
  AppointmentRepository appointmentRepo = new AppointmentRepository();

  @override
  initialise(context) async {
    selectedEvents = [];
    doctorStream = userRepo.viewStream(selectedDoctor.id);

    // Get all appointments for the nex "maxDaysToEnable"
    appointmentRepo
        .listStream(
      doctorId: selectedDoctor.id,
      days: maxDaysToEnable,
    )
        .listen((event) {
      var docs = event.docs;
      appointments = docs.map((e) => Appointment.fromDoc(e)).toList();
      updateBooked();
    });

    loggedInUser = await getLoggedInUser();
    notifyListeners();
  }

  void initialize(context, _doctor) {
    // /*******************
    //  *
    //  *
    //  * // Get UTC offset when user selects startsAt and endsAt
    //  *
    //  *
    //  * // Get UTC offset when user selects startsAt and endsAt
    //  *
    //  *
    //  * // Get UTC offset when user selects startsAt and endsAt
    //  *
    //  *********** //

    selectedDoctor = _doctor;
    // Get doctor's availability for the next 7 days

    // Get days of the next 7 days
    List<DateTime> dates = getAllDaysFromToday();
    // print(dates);
    // TimeSlot

    int timePerSlot = selectedDoctor.settings.timePerSlot;
    selectedEvents = [];

    for (DateTime date in dates) {
      // DateTime _date = DateTime.parse();
      selectedDoctor.availableOn.forEach((key, value) {
        // print(key);
        // Get the days' AvailableAt
        if (getWeekday(key) == date.weekday) {
          List<Slot> slots = [];

          // Get doctors availability
          Availability availability = Availability.fromJson(value);
          List<AvailableAt> hoursAvailable = availability.hoursAvailable;

          hoursAvailable.forEach((availableAt) {
            // AvailableAt availableAt = AvailableAt.fromJson(value);
            double totalMinutes = getTotalTime(availableAt);
            int numberOfEventsToCreate = totalMinutes.truncate() ~/ timePerSlot;

            // Get list of time based on timePerSlot
            List<String> times = [];
            DateTime startsAt = Utils.todToToday(availableAt.start);
            // print(startsAt);
            times.add(Utils.getOnlyTime(startsAt));
            for (var i = 0; i < numberOfEventsToCreate; i++) {
              times.add(
                Utils.getOnlyTime(
                  startsAt.add(Duration(minutes: timePerSlot * (i + 1))),
                ),
              );
            }

            for (var i = 0; i < numberOfEventsToCreate; i++) {
              var startsAt = times[i];
              var endsAt = times[i + 1];

              // print(startsAt);
              slots.add(
                Slot(
                  date: date,
                  startsAt: startsAt,
                  endsAt: endsAt,
                  booked: false,
                  startDate: Utils.completeTod(
                    Utils.stringToTimeOfDay2(startsAt),
                    date: date,
                  ),
                  endDate: Utils.completeTod(
                    Utils.stringToTimeOfDay2(endsAt),
                    date: date,
                  ),
                ),
              );
            }
          });

          events[date] = slots;
        }
      });
    }
    // Updatte the booked slots
    updateBooked();

    initialise(context);
  }

  @override
  void reset() {
    super.reset();
  }

  void bookAppointment(context) async {
    if (userSelectedSlots.length != 0) {
      // var slots = userSelectedSlots;
      try {
        setLoading(true);
        userSelectedSlots.forEach((slot) {
          Appointment appointment = new Appointment(
            doctorId: selectedDoctor.id,
            patientId: loggedInUser.id,
            startsAt: slot.startDate,
            endsAt: slot.endDate,
          );

          appointmentRepo.create(appointment.toJson());
        });
        setLoading(false);

        Navigator.pop(context);

        alert.show(
          message: "Appointment has been booked!",
          type: AlertType.success,
          context: context,
        );
      } catch (e) {
        print(e);
        alert.showPlatformError(
          exception: e,
          context: context,
        );
      }
      setLoading(false);
    } else {
      alert.show(
        message: "Choose at least one slot",
        type: AlertType.error,
        context: context,
      );
    }
  }

  void setAvailableSlots(List value) {
    selectedEvents = value;

    // Clear user selected slots
    userSelectedSlots.clear();
    notifyListeners();
  }

  List<DateTime> getAllDaysFromToday() {
    List<DateTime> dates = [];
    for (var i = 0; i < maxDaysToEnable; i++) {
      dates.add(today.add(Duration(days: i)));
    }

    return dates;
  }

  int getWeekday(String day) {
    var days = Utils.daysOfWeek();
    return days.indexOf(day) + 1;
  }

  double toDouble(TimeOfDay myTime) => myTime.hour + myTime.minute / 60.0;

  getTotalTime(AvailableAt availableAt) {
    TimeOfDay startTime = availableAt.start;
    TimeOfDay endTime = availableAt.end;
    double _doubleStartTime =
        startTime.hour.toDouble() + (startTime.minute.toDouble() / 60);
    double _doubleEndTime =
        endTime.hour.toDouble() + (endTime.minute.toDouble() / 60);

    double _timeDiff = _doubleEndTime - _doubleStartTime;

    int _hr = _timeDiff.truncate();
    double _minute = (_timeDiff - _timeDiff.truncate()) * 60;
    double totalMinutes = double.parse((_hr * 60).toString()) + _minute;
    return totalMinutes;
  }

  updateUserSelectedSlots(Slot slot, bool add) {
    if (add) {
      userSelectedSlots.add(slot);
    } else {
      userSelectedSlots.remove(slot);
    }
    notifyListeners();
  }

  updateBooked() {
    appointments.forEach((appointment) {
      events.forEach((key, slots) {
        if (Utils.isSameDate(key, appointment.startsAt)) {
          Slot slot = slots.singleWhere(
            (e) => e.startDate == appointment.startsAt,
            orElse: () => null,
          );
          if (slot != null) {
            slot.booked = true;
          }
        }
      });
    });
    notifyListeners();
  }
}
