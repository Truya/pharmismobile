import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';

import 'base.vm.dart';

class SettingsViewModel extends BaseViewModel {
  // Variables
  AppUser loggedInUser;

  // Repositories
  UserRepository userRepo = new UserRepository();

  @override
  initialise(context) async {
    loggedInUser = await getLoggedInUser();
    notifyListeners();
  }

  @override
  void reset() {
    super.reset();
  }
}
