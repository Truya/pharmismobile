import 'package:flutter/widgets.dart';
import 'package:truya_pharmis/models/prescription.dart';
import 'package:truya_pharmis/models/refill_request.dart';
import 'package:truya_pharmis/repositories/refill_request.repository.dart';
import 'package:truya_pharmis/services/alert.service.dart';
import 'base.vm.dart';

class RequestRefillViewModel extends BaseViewModel {
  // FormKey
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  // TextEditingControllers
  TextEditingController commentCtrl = TextEditingController();

  // Variables
  Prescription prescription;

  // Repositories
  RefillRequestRepository refillRequetRepo = new RefillRequestRepository();

  void initialize(context, Prescription _prescription) async {
    initialise(context);
    prescription = _prescription;
  }

  @override
  void reset() {
    commentCtrl.clear();
    super.reset();
  }

  void save() {
    if (this.formKey.currentState.validate()) {
      try {
        RefillRequest request = new RefillRequest(
          issuerId: prescription.issuerId,
          clinicId: prescription.clinicId,
          userId: prescription.recipientId,
          prescriptionId: prescription.id,
          comments: commentCtrl.text,
        );
        setLoading(true);
        refillRequetRepo.create(request.toJson());
        reset();
        setLoading(false);

        alert.show(
          message: "Refill request has been sent!",
          type: AlertType.success,
          context: context,
        );
        Navigator.pop(context);
      } catch (e) {
        print(e);
        alert.showPlatformError(
          exception: e,
          context: context,
        );
      }
      setLoading(false);
    }
  }
}
