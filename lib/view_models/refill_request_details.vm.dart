import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/refill_request.dart';
import 'package:truya_pharmis/repositories/refill_request.repository.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'base.vm.dart';

class RefillRequestDetailsViewModel extends BaseViewModel {
  // Variables
  RefillRequest request;
  AppUser issuer;
  AppUser user;

  AppUser loggedInUser;

  // Repositories
  RefillRequestRepository requestRepo = new RefillRequestRepository();
  UserRepository userRepo = new UserRepository();

  void initialize(context, RefillRequest _request) async {
    initialise(context);
    request = _request;

    getIssuerAndOrg();

    loggedInUser = await getLoggedInUser();
  }

  getIssuerAndOrg() async {
    var doc = await userRepo.view(request.issuerId);
    issuer = AppUser.fromDoc(doc);

    var doc2 = await userRepo.view(request.userId);
    user = AppUser.fromDoc(doc2);

    notifyListeners();
  }

  updateStatus(String status){
    request.status = status;
    notifyListeners();
  }
}
