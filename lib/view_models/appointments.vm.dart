import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/appointment.dart';
import 'package:truya_pharmis/repositories/appointment.repository.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';

import 'base.vm.dart';

class AppointmentsViewModel extends BaseViewModel {
  // Variables
  AppUser loggedInUser;

  List<Appointment> yourAppointments;
  List<DocumentSnapshot> yourAppointmentsDoc;

  List<Appointment> patientAppointments;
  List<DocumentSnapshot> patientAppointmentsDoc;

  // Repositories
  AppointmentRepository appointmentRepo = new AppointmentRepository();
  UserRepository userRepo = new UserRepository();

  @override
  initialise(context) async {
    yourAppointments = [];
    yourAppointmentsDoc = [];

    patientAppointments = [];
    patientAppointmentsDoc = [];

    loggedInUser = await getLoggedInUser();

    index();
    notifyListeners();
  }

  @override
  void reset() {
    super.reset();
  }

  void yourAppointmentsReset() {
    yourAppointments = [];
    yourAppointmentsDoc = [];

    notifyListeners();
  }

  void patientsAppointmentsReset() {
    patientAppointments = [];
    patientAppointmentsDoc = [];

    notifyListeners();
  }

  void yourAppointmentsRefresh() async {
    yourAppointmentsReset();
    getYourAppointments();
  }

  void patientsAppointmentsRefresh() async {
    patientsAppointmentsReset();
    getPatientsAppointments();
  }

  index() {
    getPatientsAppointments();
    getYourAppointments();
  }

  Future getYourAppointments() async {
    setListLoading(true);
    QuerySnapshot data = await appointmentRepo.list(
      patientId: appointmentRepo.auth.currentUser.uid,
      // status: selectedStatus,
    );
    setListLoading(false);
    yourAppointmentsDoc.addAll(data.docs);
    yourAppointments.addAll(
      yourAppointmentsDoc.map((e) => Appointment.fromDoc(e)).toList(),
    );
    notifyListeners();
  }

  Future getPatientsAppointments() async {
    setListLoading(true);
    QuerySnapshot data = await appointmentRepo.list(
      doctorId: appointmentRepo.auth.currentUser.uid,
      // status: selectedStatus,
    );
    setListLoading(false);
    patientAppointmentsDoc.addAll(data.docs);
    patientAppointments.addAll(
      patientAppointmentsDoc.map((e) => Appointment.fromDoc(e)).toList(),
    );
    notifyListeners();
  }
}
