import 'package:flutter/material.dart';
import 'package:truya_pharmis/models/app_user.dart';
import 'package:truya_pharmis/models/available_at.dart';
import 'package:truya_pharmis/models/user_settings.dart';
import 'package:truya_pharmis/repositories/user.repository.dart';
import 'package:truya_pharmis/services/alert.service.dart';

import 'base.vm.dart';

class AppointmentSettingsViewModel extends BaseViewModel {
  // TextEditingControllers
  TextEditingController unitsPerSlotCtrl = TextEditingController();

  // Variables
  AppUser loggedInUser;
  List<String> daysOfTheWeek = [
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday",
    "sunday"
  ];

  List<bool> available;

  List<int> timeslots = [10, 15, 20, 30, 45];
  int selectedTimePerSlot = 10;

  // Repositories
  UserRepository userRepo = new UserRepository();

  @override
  initialise(context) async {
    resetAvailable();
    loggedInUser = await getLoggedInUser();
    loggedInUser.availableOn.forEach((key, value) {
      int index = daysOfTheWeek.indexOf(key);
      available[index] = true;
    });
    selectedTimePerSlot = loggedInUser.settings.timePerSlot;
    unitsPerSlotCtrl.text = loggedInUser.settings.unitsPerSlot.toString();
    notifyListeners();
  }

  @override
  void reset() {
    super.reset();
  }

  void updateSettings(_context) {
    if (unitsPerSlotCtrl.text != null) {
      var userSettings = UserSettings(
        timePerSlot: selectedTimePerSlot,
        unitsPerSlot: int.parse(unitsPerSlotCtrl.text),
      );
      userRepo.update(
        userRepo.auth.currentUser.uid,
        Map<String, dynamic>.from({"settings": userSettings.toJson()}),
      );
      alert.show(
        message: "Settings updated",
        type: AlertType.success,
        context: context,
      );
      loggedInUser.settings.timePerSlot = selectedTimePerSlot;
      loggedInUser.settings.unitsPerSlot = int.parse(unitsPerSlotCtrl.text);
      notifyListeners();
      Navigator.pop(_context);
    }
  }

  void updateAvailability(
    _context,
    bool available,
    String day, {
    AvailableAt availableAt,
  }) {
    var currentUserAvailableOn = loggedInUser.availableOn;
    if (available) {
      currentUserAvailableOn[day] = availableAt.toJson();
    } else {
      currentUserAvailableOn[day] = null;
      currentUserAvailableOn.removeWhere((key, value) => key == day);
    }
    userRepo.update(
      userRepo.auth.currentUser.uid,
      Map<String, dynamic>.from({
        "availableOn": currentUserAvailableOn,
      }),
    );
    alert.show(
      message: "Appointment settings updated",
      type: AlertType.success,
      context: context,
    );
    updateUser();
    Navigator.pop(_context);
  }

  void setAvailable() {}

  void setTimePerSlot(int value) {
    selectedTimePerSlot = value;
    notifyListeners();
  }

  void updateUser() async {
    resetAvailable();
    loggedInUser = await getLoggedInUser();
    loggedInUser.availableOn.forEach((key, value) {
      int index = daysOfTheWeek.indexOf(key);
      available[index] = true;
    });
    selectedTimePerSlot = loggedInUser.settings.timePerSlot;
    unitsPerSlotCtrl.text = loggedInUser.settings.unitsPerSlot.toString();
    notifyListeners();
  }

  void resetAvailable() {
    available = [false, false, false, false, false, false, false];
    notifyListeners();
  }
}
