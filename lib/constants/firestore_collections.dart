class FirestoreCollections {
  static const String users = "users";
  static const String prescriptions = "prescriptions";
  static const String organizations = "organizations";
  static const String refillRequests = "refillRequests";
  static const String appointments = "appointments";
  static const String callLogs = "callLogs";
}
