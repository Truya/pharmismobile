class AppImages {
  static const String logo = 'assets/images/stable_doc_logo.png';
  static const String truyaLogo = 'assets/images/truya_logo.png';
  static const String empty = 'assets/images/empty.png';
  static const String googlLogo = 'assets/images/google_logo.png';
  static const String facebookLogo = 'assets/images/facebook_logo.png';
  static const String appleLogo = 'assets/images/apple_logo.png';

  // icons
  static const String homeIcon = 'assets/icons/home.png';
  static const String doctorIcon = 'assets/icons/doctor.png';
  static const String prescriptionsIcon = 'assets/icons/prescriptions.png';
  static const String refillIcon = 'assets/icons/refill.png';
  static const String profileIcon = 'assets/icons/profile.png';

  // Categories
  static const String dentalSurgeon = 'assets/icons/tooth.png';
  static const String heartSurgeon = 'assets/icons/health.png';
  static const String eyeSpecialist = 'assets/icons/eye.png';
}
